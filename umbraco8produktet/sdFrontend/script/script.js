$(document).ready(function () {

  //Burger menu
  $('.site-header__burger').on('click', function () {
    $(this).toggleClass('change');
    $('.site-header__nav').toggleClass('element-show');
    $('html').toggleClass('stop-scroll');
  });

  //Onclick show search
  $('.site-header__search-icon #search-icon').on('click', function () {
    var src = ($(this).attr('src') === 'assets/images/search-icon-white.svg')
      ? 'assets/images/close-white.png'
      : 'assets/images/search-icon-white.svg';
    $(this).attr('src', src);
    $('.site-header__search').slideToggle();
  });

  $('.site-header__search-icon #search-icon-phone').on('click', function () {
    $('.site-header__search').slideToggle();
  });

  $('.site-header__search-close').on('click', function () {
    $('.site-header__search').slideToggle();
  });

  //Hero slider
  $('.hero-slider').slick({
    arrows: false,
    autoplay: true,
    speed: 500
  });

  //Single product slider
  $('.single-product__slider').slick({
    arrows: true,
    autoplay: true,
    speed: 500
  });

  //Filter partners
  function tag() {
    $('.but-alle').on('click', function () {
      $('.product-box').show('fast');
    });
    $('.but-fyn').on('click', function () {
      $('.product-box').hide();
      $('.fyn').show('fast');
    });
    $('.but-jylland').on('click', function () {
      $('.product-box').hide();
      $('.jylland').show('fast');
    });
    $('.but-sjaland').on('click', function () {
      $('.product-box').hide();
      $('.sjaland').show('fast');
    });
  };

  $(function () {
    tag();
  });

  // Filter products
  function tag1() {
    $('.btn-alle').on('click', function () {
      $('.product-box').hide();
      $('.erhverv, .privat, .offentlig, .industri').show('fast');
    });
    $('.btn-erhverv').on('click', function () {
      $('.product-box').hide();
      $('.erhverv').show('fast');
    });
    $('.btn-privat').on('click', function () {
      $('.product-box').hide();
      $('.privat').show('fast');
    });
    $('.btn-offentlig').on('click', function () {
      $('.product-box').hide();
      $('.offentlig').show('fast');
    });
    $('.btn-industri').on('click', function () {
      $('.product-box').hide();
      $('.industri').show('fast');
    });
  };

  $(function () {
    tag1();
  });

  //Contact form validation
  if (window.location.pathname=='/contact.html') {
    $("#commentForm").validate({
      success: "valid",
      submitHandler: function () {
        alert("Submitted!")
      }
    });
  }
  
});