const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const minifyCss = require('gulp-clean-css');
const browserSync = require('browser-sync').create();

function style() {
  return gulp.src('./styles/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./styles/css'))
    .pipe(browserSync.stream());
}

function minifyStyle() {
  return gulp.src('./styles/css/**/*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('./dist'));
}

function scriptConcat() {
  return gulp.src('./script/**/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./dist/'))
}

function watch() {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
  gulp.watch('./styles/scss/**/*.scss', style);
  gulp.watch('./styles/css/styles.css', minifyStyle);
  gulp.watch('./script/**/*.js', scriptConcat);
  gulp.watch('./*.html').on('change', browserSync.reload);
  gulp.watch('./script/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.minifyStyle = minifyStyle;
exports.scriptConcat = scriptConcat;
exports.watch = watch;