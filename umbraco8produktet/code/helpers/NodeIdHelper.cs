﻿using code.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace code.helpers
{
    class NodeIdHelper
    {

        public static CustomerInfo GetCustomerInfoByDomain(string domain = "localhost")
        {

            CustomerInfo c = new CustomerInfo();

            string customerJson = string.Empty;
            customerJson = File.ReadAllText(HttpContext.Current.Server.MapPath("~/customerInfo.json"), Encoding.Default);
            List<CustomerInfo> customerlist = JsonConvert.DeserializeObject<List<CustomerInfo>>(customerJson);

            c = customerlist.FirstOrDefault(x => x.domain == domain) ?? 
                customerlist.FirstOrDefault(x => x.domain == "localhost");

            return c;
        }

        public class CustomerInfo
        {
            public string domain { get; set; }
            public int rootNodeId { get; set; }
            public string[] ScriptHead { get; set; }
            public string[] ScriptBodyTop { get; set; }
            public string[] ScriptBodyBottom { get; set; }
            public ComponentsSections menu { get; set; }
            public ComponentsSections layout { get; set; }
            public ComponentsSections header { get; set; }
            public ComponentsSections footer { get; set; }
            public ComponentsSections content { get; set; }
            public string[] Styles { get; set; }
            public string[] Scripts { get; set; }



        }

    }
}



