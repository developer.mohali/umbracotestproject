﻿using code.models;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace code.helpers
{
    class GeneralDataHelper
    {
        public static GeneralData GetGeneralData(IPublishedContent node)
        {
            GeneralData g = new GeneralData();
            g.CompanyName = node.Value<string>("CompanyName").ToString();
            g.CompanyAddress = node.Value<string>("CompanyAddress").ToString();
            g.CompanyAddress2 = node.Value<string>("CompanyAddress2").ToString();
            g.CompanyPhone = node.Value<string>("CompanyPhone").ToString();
            g.CompanyEmail = node.Value<string>("CompanyEmail").ToString();
            g.MainLogo = node.Value<string>("mainLogo").ToString();
            g.CompanyFacebook = node.Value<string>("CompanyFacebook").ToString();
            g.CompanyInstagram = node.Value<string>("CompanyInstagram").ToString();
            g.CompanyLinkedin = node.Value<string>("CompanyLinkedin").ToString();
            g.CompanyTwitter = node.Value<string>("CompanyTwitter").ToString();
            g.CompanyDescription = node.Value<string>("CompanyDescription").ToString();

            var links = node.Value<IEnumerable<Link>>("FooterLinks");
            List<FooterLink> f = new List<FooterLink>();
            if (links.Any())
            {
                foreach (var link in links)
                {
                    f.Add(new FooterLink()
                    {
                        Name = link.Name,
                        Url = link.Url,
                        Target = link.Target
            
                    });
                }
            }

            g.FooterLinks = f;

            // wait
            g.ScriptHead = new string[] { };
            g.ScriptBodyTop = new string[] { };
            g.ScriptBodyBottom = new string[] { };

            return g;
        }
    }
}
