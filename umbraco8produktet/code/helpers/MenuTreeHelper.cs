﻿using code.models;
using System.Collections.Generic;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace code.helpers
{
    public class MenuTreeHelper
    {

        public static Menus getMenuItem(IPublishedContent publishedContent, bool includeChildren)
        {

            var result = new Menus()
            {
                menuIcon = null,
                menuImg = null,
                menuMp4 = null,
                menuWebm = null,
                name = publishedContent.Name,
                navigationHide = !publishedContent.IsVisible(),
                path = publishedContent.Url.TrimStart('/').TrimEnd('/'),
                metaDescription = publishedContent.Value<string>("SeoDescription"),
                metaKeywords = "",
                metaImage = publishedContent.Value<string>("ogImage"),
                metaTitle = publishedContent.Value<string>("seoTitle"),
                favicon = publishedContent.Value<string>("favicon", fallback: Fallback.ToAncestors),
                content = ComponentHelper.GenerateComponentArray(publishedContent, "indhold")
            };

            if (includeChildren)
            {
                result.subMenu = getMenuItemsChildren(publishedContent);
            }

            return result;
        }

        public static List<Menus> getMenuItemsChildren(IPublishedContent publishedContent)
        {
            var result = new List<Menus>();

            foreach (var item in publishedContent.Children)
            {
                result.Add(getMenuItem(item, true));
            }

            return result;
        }

    }
}
