﻿using AutoMapper;
using System;
using System.Linq;
using System.Runtime.Caching;
using Umbraco.Core;

namespace code.helpers
{
    public static class MemoryCacher
    {
        public static object GetValue(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(key);
        }

        public static bool Add(string key, object value, DateTimeOffset absExpiration)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Add(key, value, absExpiration);
        }

        public static void Delete(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }

        public static void Clear()
        {
            MemoryCache memoryCache = MemoryCache.Default;
            memoryCache.Where(x => x.Key.Contains("GetTree")).Each(x => x.DisposeIfDisposable());
        }
    }
}
