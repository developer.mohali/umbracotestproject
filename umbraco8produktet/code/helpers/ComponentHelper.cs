﻿using code.models;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace code.helpers
{
    class ComponentHelper
    {
        public static List<ComponentsValues> GenerateComponentArray(IPublishedContent node, string id)
        {
            List<ComponentsValues> c = new List<ComponentsValues>();
            var nestedArray = node.Value<IEnumerable<IPublishedElement>>(id);

            foreach (var publishedElement in nestedArray)
            {
                if (publishedElement.ContentType.Alias.Equals("tekst"))
                {
                    c.Add(new Text()
                    {
                        name = "text",
                        key = publishedElement.Key.ToString(),
                        markup = publishedElement.Value<string>("SkrivTekst"),
                        nodeid = node.Id
                    });
                }

                // not used yet !!
                //if (publishedElement.ContentType.Alias.Equals("grid"))
                //{
                //    c.Add(new ComponentsValues()
                //    {
                //        name = "grid",
                //        key = publishedElement.Key.ToString(),
                //        markup = publishedElement.Value<string>("bygIndhold"),
                //        nodeid = node.Id
                //    });
                //}

                if (publishedElement.ContentType.Alias.Equals("billede"))
                {
                    c.Add(new Image()
                    {
                        name = "image",
                        key = publishedElement.Key.ToString(),
                        src = publishedElement.HasValue("vaegleBillede") ? publishedElement.Value<IPublishedContent>("vaegleBillede").Url : string.Empty,
                        alt = publishedElement.Value<string>("alt"),
                        nodeid = node.Id
                    });
                }

                if (publishedElement.ContentType.Alias.Equals("video"))
                {
                    bool isMuted = false;
                    bool isAutoplay = false;
                    if (publishedElement.Value<bool>("autoplayOgMute"))
                    {
                        isMuted = true;
                        isAutoplay = true;
                    }

                    bool showControls = true;
                    if (publishedElement.Value<bool>("skjulKontrolpanel"))
                    {
                        showControls = false;
                    }

                    c.Add(new Video()
                    {
                        name = "video",
                        key = publishedElement.Key.ToString(),
                        src = publishedElement.HasValue("billede") ? publishedElement.Value<IPublishedContent>("billede").Url : string.Empty,
                        mp4Url = publishedElement.HasValue("mp4Video") ? publishedElement.Value<IPublishedContent>("mp4Video").Url : string.Empty,
                        oggUrl = publishedElement.HasValue("oggVideo") ? publishedElement.Value<IPublishedContent>("oggVideo").Url : string.Empty,
                        isAutoplay = isAutoplay,
                        isLoop = false,
                        isMuted = isMuted,
                        showControls = showControls,
                        nodeid = node.Id
                    });
                }

                if (publishedElement.ContentType.Alias.Equals("banner"))
                {
                    string DomainName = "";
                    if (HttpContext.Current.Request.Url.Port.ToString() == "52558")
                    {
                        DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port);
                    }
                    else
                    {
                        DomainName = string.Format("https://{0}", HttpContext.Current.Request.Url.Host);
                    }

                    string overlayMarkup = string.Empty;
                    if (!string.IsNullOrEmpty(publishedElement.Value<string>("overlayOverskrift")))
                    {
                        overlayMarkup = "<h1>" + publishedElement.Value<string>("overlayOverskrift") + "</h1>";
                    }
                    if (!string.IsNullOrEmpty(publishedElement.Value<string>("overlayTekst")))
                    {
                        overlayMarkup += publishedElement.Value<string>("overlayTekst");
                    }


                    c.Add(new Banner()
                    {
                        name = "banner",
                        nodeid = node.Id,
                        key = publishedElement.Key.ToString(),
                        src = publishedElement.HasValue("billede") ? DomainName + publishedElement.Value<IPublishedContent>("billede").Url : string.Empty,
                        mp4Url = publishedElement.HasValue("mp4Video") ? publishedElement.Value<IPublishedContent>("mp4Video").Url : string.Empty,
                        oggUrl = publishedElement.HasValue("oggVideo") ? publishedElement.Value<IPublishedContent>("oggVideo").Url : string.Empty,
                        overlayMarkup = overlayMarkup,
                        bgColor = publishedElement.Value<string>("baggrundsfarve"),
                        posterSrc = publishedElement.HasValue("posterBillede") ? publishedElement.Value<IPublishedContent>("posterBillede").Url : string.Empty,
                        displayOverlay = publishedElement.Value<bool>("skjulOverlay"),
                        fullOverlay = publishedElement.Value<bool>("fuldtOverlay")

                     });
                }

                if (publishedElement.ContentType.Alias.Equals("teaser"))
                {
                    string DomainName = "";
                    if (HttpContext.Current.Request.Url.Port.ToString() == "52558")
                    {
                        DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port);
                    }
                    else
                    {
                        DomainName = string.Format("https://{0}", HttpContext.Current.Request.Url.Host);
                    }

                    string markup = string.Empty;
                    if (!string.IsNullOrEmpty(publishedElement.Value<string>("overskrift")))
                    {
                        markup = "<h2>" + publishedElement.Value<string>("overskrift") + "</h2>";
                    }
                    if (!string.IsNullOrEmpty(publishedElement.Value<string>("tekst")))
                    {
                        markup += publishedElement.Value<string>("tekst");
                    }


                    c.Add(new Teaser()
                    {
                        name = "teaser",
                        nodeid = node.Id,
                        key = publishedElement.Key.ToString(),
                        src = publishedElement.HasValue("billede") ? publishedElement.Value<IPublishedContent>("billede").Url : string.Empty,
                        markup = markup,
                        sortingLeftRight = publishedElement.Value<bool>("billedeVenstreTekstHojre")

                    });
                }

                if (publishedElement.ContentType.Alias.Equals("slider"))
                {

                    string DomainName = "";
                    if (HttpContext.Current.Request.Url.Port.ToString() == "52558")
                    {
                        DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port);
                    }
                    else
                    {
                        DomainName = string.Format("https://{0}", HttpContext.Current.Request.Url.Host);
                    }


                    var imgs = new List<string>();

                    var imagesPicker = publishedElement.Value<IEnumerable<IPublishedContent>>("billeder");
                    if (imagesPicker != null)
                    {
                       foreach (var item in imagesPicker)
                       {
                           imgs.Add(DomainName + item.Url);
                       }
                    }

                    c.Add(new Slider()
                    {
                        name = "slider",
                        key = publishedElement.Key.ToString(),
                        nodeid = node.Id,
                        images = imgs.ToArray(),
                        displayArrows = publishedElement.Value<bool>("visPile"),
                        displayDots = publishedElement.Value<bool>("visDots")
                    });
                };

                if (publishedElement.ContentType.Alias.Equals("statments"))
                {
                    // Why am I not able to render nested content inside nested content like here:
                    List<StatmentChild> el = new List<StatmentChild>();

                    //var elementTyped = publishedElement as Statments;


                    var statmentChildren = (IEnumerable<IPublishedElement>)publishedElement.Value("neselm");
                    //var statmentChildren = JsonConvert.DeserializeObject();

                    if (statmentChildren != null)
                    {
                        foreach (var item in statmentChildren)
                        {
                            el.Add(new StatmentChild()
                            {
                                alias = item.GetProperty("alias") != null ? item.GetProperty("alias").GetValue().ToString() : string.Empty,
                                text = item.GetProperty("tekst") != null ? item.GetProperty("tekst").GetValue().ToString() : string.Empty,
                                iconClass = item.GetProperty("ikon") != null ? item.GetProperty("ikon").GetValue().ToString() : string.Empty
                            });
                        }
                    }

                    c.Add(new Statment()
                    {
                       
                   
                        name = "feature",
                        key = publishedElement.Key.ToString(),
                        nodeid = node.Id,
                        heading = publishedElement.Value<string>("overskrift"),
                        textCenter = publishedElement.Value<bool>("centrerTekster"),
                        elements = el
                    });
                };

                if (publishedElement.ContentType.Alias.Equals("anbefalinger"))
                {

                    List<TestimonialChild> el = null;

                    //var statmentChildren = (IEnumerable<IPublishedElement>)publishedElement.Value("elementer");

                    c.Add(new Testimonials()
                    {


                        name = "testimonial",
                        key = publishedElement.Key.ToString(),
                        nodeid = node.Id,
                        designShuffle = publishedElement.Value<bool>("skiftMellemBilledeTekst"),
                        elements = el
                    });
                };

            }

            return c;
        }
    }
}
