﻿using System.Collections.Generic;

namespace code.models
{

    public class Root
    {
        public SiteModel localhost_will_be_dynamic_domainname { get; set; }
        public GeneralData getgeneralData { get; set; }
        public List<Menus> menus { get; set; }

        public Root()
        {
            localhost_will_be_dynamic_domainname = new SiteModel();
            getgeneralData = new GeneralData();
            menus = new List<Menus>();
            menus.Add(new Menus() { content = new List<ComponentsValues>()});
        }
    }

    public class SiteModel
    {
        public int pageId { get; set; }
        public List<ComponentsValues> menu { get; set; }
        public List<ComponentsValues> layout { get; set; }
        public List<ComponentsValues> header { get; set; }
        public List<ComponentsValues> footer { get; set; }
        public List<ComponentsValues> content { get; set; }
        public string[] scripts { get; set; }
        public string[] styles { get; set; }

        public SiteModel()
        {
            pageId = 1104;
            menu = new List<ComponentsValues>();
            menu.Add(new ComponentsValues());
            layout = new List<ComponentsValues>();
            layout.Add(new ComponentsValues());
            header = new List<ComponentsValues>();
            header.Add(new ComponentsValues());
            footer = new List<ComponentsValues>();
            footer.Add(new ComponentsValues());
            content = new List<ComponentsValues>();
            content.Add(new ComponentsValues());
            scripts = new[] { "scripts", "scripts", "scripts" };
            styles = new[] { "styles", "styles", "styles" };
        }
        
    }

    public class GeneralData
    {
        public string CompanyName { get; set; }
		public string CompanyAddress { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string MainLogo { get; set; }
        public string CompanyFacebook { get; set; }
        public string CompanyLinkedin { get; set; }
        public string CompanyInstagram { get; set; }
        public string CompanyTwitter { get; set; }
        public string CompanyDescription { get; set; }
        public FooterLink FooterLinks { get; set; }
        public string[] ScriptHead { get; set; }
        public string[] ScriptBodyTop { get; set; }
        public string[] ScriptBodyBottom { get; set; }

        public GeneralData()
        {
            CompanyName = "Q-Construction";
            CompanyAddress = "Testvej 20";
            CompanyAddress2 = "8000 Aarhus C";
            CompanyPhone = "+45 21332323";
            CompanyEmail = "info@q-c.dk";
            MainLogo = "http://produktetapi.dynamikfabrikken.com:80/media/1065/q-construction-logo.png";
            CompanyFacebook = null;
            CompanyLinkedin = "https://linkeding.com";
            CompanyInstagram = "https://instagram";
            CompanyTwitter = "https://twitter.com";
            CompanyDescription = "Lorem ipsum dolor sit amet, et parturient eros, at tempus odio sed sed ut et, ali";
            FooterLinks = new FooterLink();
            ScriptHead = new[] { "ScriptHead1", "ScriptHead2", "ScriptHead3" };
            ScriptBodyTop = new[] { "ScriptBodyTop", "ScriptBodyTop", "ScriptBodyTop" };
            ScriptBodyBottom = new[] { "ScriptBodyBottom", "ScriptBodyBottom", "ScriptBodyBottom" };
        }
    }

    public class Menus
    {
        public string name { get; set; }
        public string subname { get; set; }
        public string Name { get; set; }
        public string path { get; set; }
        public bool navigationHide { get; set; }
        public string menuIcon { get; set; }
        public string menuImg { get; set; }
        public string menuMp4 { get; set; }
        public string menuWebm { get; set; }
        public IEnumerable<Menus> subMenu { get; set; }
        public List<ComponentsValues> content { get; set; }

        public Menus()
        {   
            name = null;
            subname = null;
            path = null;
            navigationHide = false;
            menuIcon = null;
            menuMp4 = null;
            menuWebm = null;
            subMenu = null;
            content = new List<ComponentsValues>();
            content.Add(new ComponentsValues());
        }

    }

    public class ComponentsValues
    {
        //Value will be some markup- If no Value we will make request from Angular component and ask for content from key + nodeid like grid
        public string name { get; set; }
        public string markup { get; set; }
        public string key { get; set; }
        public int nodeid { get; set; }

        public ComponentsValues()
        {
            name = "text";
            markup = "<p>If the nested content is image or RTE etc. we will provide markup in value. If grid or slider we provide key and nodeid instead</p>";
            key = "key for content goes here";
            nodeid = 0;

        }
    }

    public class FooterLink
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }

        public FooterLink() {
            Name = "Link1";
            Url = "/link1";
            Target = "_blank";
        }
    }
}
