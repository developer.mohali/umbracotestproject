﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace code.models
{
    public class NodeListsModel
    {
        [JsonProperty("nodelist")]
        public object nodelist { get; set; } 

        public NodeListsModel()
        {
        }

        public NodeListsModel(IPublishedContent content)
        {
            nodelist = null;
        }
    }
}