﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace code.models.helper
{
    public class ImageModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("croppedUrl")]
        public string CroppedUrl { get; set; }

        [JsonProperty("altText")]
        public string AltText { get; set; }

        private static readonly string DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port); //HttpContext.Current.Request.Url.Host

        private static readonly UmbracoHelper _helper = new UmbracoHelper(UmbracoContext.Current);

        public static ImageModel GetImage(IPublishedContent content, string property, int width = 800, int height = 600)
        {
            var image = content.TypedCsvMedia(property).FirstOrDefault();

            if (image == null)
                return null;

            return new ImageModel
            {
                Id = image.Id,
                Url = DomainName + image.Url,
                CroppedUrl = DomainName + image.GetCropUrl(width, height, preferFocalPoint: true, imageCropMode: ImageCropMode.Crop),
                AltText = image.HasValue("alttext") ? image.GetPropertyValue<string>("alttext") : ""
            };
        }


        public static IEnumerable<ImageModel> GetImages(IPublishedContent content, string property, int width = 800, int height = 600)
        {
            var imagesData = content.TypedCsvMedia(property);

            if (imagesData == null)
                return null;

            return imagesData.Select(m => new ImageModel
            {
                Id = m.Id,
                Url = DomainName + m.Url,
                CroppedUrl = DomainName + m.GetCropUrl(width, height, preferFocalPoint: true, imageCropMode: ImageCropMode.Crop),
                AltText = m.HasValue("alttext") ? m.GetPropertyValue<string>("alttext") : ""
            }).ToList();
        }

        public static ImageModel GetUiImageCrop(IContentBase imgdata, string property, int width = 800, int height = 600)
        {
            var imageNode = _helper.TypedContent(imgdata.Id);

            return new ImageModel
            {
                Id = imgdata.Id,
                Url = DomainName + imageNode.GetCropUrl(propertyAlias: property),
                CroppedUrl = DomainName + imageNode.GetCropUrl(width, height, preferFocalPoint: true, imageCropMode: ImageCropMode.Crop, propertyAlias: property)
                //AltText = imageData.HasValue("alttext") ? imageData.GetPropertyValue<string>("alttext") : ""
            };
        }

    }
}
