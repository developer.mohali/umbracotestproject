﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Umbraco.Web.Templates;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web.WebApi;



namespace code.models.helper
{
    public class GridModel
    {

        public static string GetGridHtml(string json)
        {
            dynamic jsonObj = JsonConvert.DeserializeObject(json);

            string htmlString = "";
            if (jsonObj != null && jsonObj.sections != null)
            {
                var oneColumn = ((System.Collections.ICollection)jsonObj.sections).Count == 1;

                htmlString = "<div class='umb-grid'>";
                if (oneColumn)
                {
                    foreach (var section in jsonObj.sections)
                    {
                        htmlString += "<div class='grid-section'>";
                        foreach (var row in section.rows)
                        {
                            htmlString += renderRow(row, true);
                        }
                        htmlString += "</div>";
                    }
                }
                else
                {
                    htmlString += "<div class='container'>";
                    htmlString += "<div class='row clearfix'>";
                    foreach (var s in jsonObj.sections)
                    {
                        htmlString += "<div class='grid-section'>";
                        htmlString += "<div class='col s1" + s.grid + "'>";
                        foreach (var row in s.rows)
                        {
                            htmlString += renderRow(row, false);
                        }
                        htmlString += "</div>";
                        htmlString += "</div>";
                    }
                    htmlString += "</div>";
                    htmlString += "</div>";
                }
                htmlString += "</div>";
            }

            return htmlString;
        }

        private static string renderRow(dynamic row, bool singleColumn)
        {
            string rowHtmlString = "";

            rowHtmlString += "<div " + RenderElementAttributes(row) + ">";
            if (singleColumn)
            {
                rowHtmlString += "<div class='container'>";
            }
            rowHtmlString += "<div class='row clearfix'>";
            foreach (var area in row.areas)
            {
                rowHtmlString += "<div class='col s" + area.grid + "'>";
                rowHtmlString += "<div " + RenderElementAttributes(area) + ">";
                foreach (var control in area.controls)
                {
                    if (control != null && control.editor != null && control.editor.view != null)
                    {
                        rowHtmlString += EditorView((object)control);
                    }
                }
                rowHtmlString += "</div>";
                rowHtmlString += "</div>";
            }
            rowHtmlString += "</div>";
            if (singleColumn)
            {
                rowHtmlString += "</div>";
            }
            rowHtmlString += "</div>";

            return rowHtmlString;
        }

        public static MvcHtmlString RenderElementAttributes(dynamic contentItem)
        {
            var attrs = new List<string>();
            JObject cfg = contentItem.config;

            if (cfg != null)
                foreach (JProperty property in cfg.Properties())
                {
                    attrs.Add(property.Name + "='" + property.Value.ToString().ToLower().Replace(" ", "-") + "'");
                }

            JObject style = contentItem.styles;

            if (style != null)
            {
                var cssVals = new List<string>();
                string DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port);

                foreach (JProperty property in style.Properties())
                    cssVals.Add(property.Name + ":" + property.Value.ToString() + ";");

                if (cssVals.Any())
                    attrs.Add("style='" + string.Join(" ", cssVals).Replace("background-image:url(", "background-image:url(" + DomainName) + "'");
            }

            return new MvcHtmlString(string.Join(" ", attrs));
        }

        private static string EditorView(dynamic contentItem)
        {
            try
            {
                string view = contentItem.editor.render != null ? contentItem.editor.render.ToString() : contentItem.editor.view.ToString();
                view = view.ToLower().Replace(".html", ".cshtml");

                if (!view.Contains("/"))
                {
                    if (view == "rte")
                    {
                        view = TemplateUtilities.ParseInternalLinks(contentItem.value.ToString().Replace("src=\"/", "src=\"http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/"));
                    }
                    else if (view == "textstring")
                    {
                        if (contentItem.editor.config.markup != null)
                        {
                            string markup = contentItem.editor.config.markup.ToString();

                            markup = markup.Replace("#value#", contentItem.value.ToString());
                            markup = markup.Replace("#style#", contentItem.editor.config.style.ToString());

                            view = markup;
                        }
                        else
                        {
                            view = "<div style='" + contentItem.editor.config.style + "'>" + contentItem.value + "</div>";
                        }
                    }
                    else if (view == "media")
                    {
                        if (contentItem.value != null)
                        {
                            string domainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port);
                            var url = contentItem.value.image;
                            if (contentItem.editor.config != null && contentItem.editor.config.size != null)
                            {
                                url += "?width=" + contentItem.editor.config.size.width;
                                url += "&height=" + contentItem.editor.config.size.height;

                                if (contentItem.value.focalPoint != null)
                                {
                                    url += "&center=" + contentItem.value.focalPoint.top + "," + contentItem.value.focalPoint.left;
                                    url += "&mode=crop";
                                }
                            }

                            view = "<img src='" + domainName + url + "' alt='" + contentItem.value.caption + "'>";

                            if (contentItem.value.caption != null)
                            {
                                view += "<p class='caption'>" + contentItem.value.caption + "</p>";
                            }
                        }
                    }
                    else if (view == "embed")
                    {
                        view += "<div class='video-wrapper'>";
                        view += contentItem.value;
                        view += "</div>";
                    }
                    else if (view == "macro")
                    {
                        if (contentItem.value != null)
                        {
                            string macroAlias = contentItem.value.macroAlias.ToString();
                            ViewDataDictionary parameters = new ViewDataDictionary();
                            foreach (dynamic mpd in contentItem.value.macroParamsDictionary)
                            {
                                parameters.Add(mpd.Name, mpd.Value);
                            }

                            //
                            //macro types
                            if (macroAlias == "cardlist")
                            {
                                view = buildcards(parameters);
                            }
                            else if (macroAlias == "slider")
                            {
                                view = buildslider();
                            }
                            else if (macroAlias == "Form")
                            {
                                view = "";
                                view += "<div class='col s12'>";
                                view += "<h1>This will tell to render form and functionality with id: 1 here</h1>";
                                view += "{{renderformid_1}}";
                                view += "</div>";
                                
                            }


                        }

                    }
                }
                else if (view == "/app_plugins/grid/editors/render/embed_videowrapper.cshtml")
                {
                    view = "<div class='video-wrapper'>";
                    view += contentItem.value.ToString().Replace("\"", "'");
                    view += "</div>";
                }

                return view;
            }
            catch (System.Exception ex)
            {
                return "<pre>" + ex.ToString() + "</pre>";
            }
        }


        public static string buildslider()
        {
            var slider = "";
            slider += "<div materialize='carousel' class='carousel'>";
            
            slider += "<div class='carousel-fixed-item center'>";
            slider += "<a class='btn waves-effect white grey-text darken-text-2'>button</a>";
            slider += "</div>";

            for (int i = 0; i < 3; i++)
            {
                slider += "<div class='carousel-item red white-text' href='#" + i + "'>";
                slider += "<h2>First Panel</h2><p class='white-text'>This is your first panel</p>";
                slider += "</div>";
                //slider += "<a class='carousel-item' href='#" + i + "'><img src='http://lorempixel.com/250/250/nature/1'></a>";
            }
            slider += "</div>";


            slider += "<div appCarouselSpy id='full-carousel' class='carousel carousel-slider center' data-indicators='true'>";
              slider += "<div class='carousel-fixed-item center'>";
                  slider += "<a class='btn waves-effect white grey-text darken-text-2'>button</a>";
              slider += "</div>";
              slider += "<div class='carousel-item red white-text' href='#one!'>";
                  slider += "<h2>First Panel</h2>";
                  slider += "<p class='white-text'>This is your first panel</p>";
              slider += "</div> ";
              slider += "<div class='carousel-item amber white-text' href='#two!'>";
                  slider += "<h2>Second Panel</h2>";
                  slider += "<p class='white-text'>This is your second panel</p>";
              slider += "</div>";
              slider += "<div class='carousel-item green white-text' href='#three!'>";
                  slider += "<h2>Third Panel</h2>";
                  slider += "<p class='white-text'>This is your third panel</p>";
              slider += "</div>";
              slider += "<div class='carousel-item blue white-text' href='#four!'>";
                  slider += "<h2>Fourth Panel</h2>";
                  slider += "<p class='white-text'>This is your fourth panel</p>";
              slider += "</div>";
              slider += "</div>";




            return slider;
        }

        public static string buildcards(ViewDataDictionary cardlistparameters)
        {
            var cards = "";
            cards += "<div class='row'>";

            foreach (var item in cardlistparameters)
            {
                var id = ApplicationContext.Current.Services.ContentService.GetById(int.Parse(item.Value.ToString()));
                foreach (var p in id.Children())
                {

                }
            }

            cards += "</div>";

            return cards;
        }

    }

}
