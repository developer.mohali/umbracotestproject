﻿﻿using System.Collections.Generic;

namespace code.models
{

    public class Root
    {
        public SiteModel localhost_will_be_dynamic_domainname { get; set; }
        public GeneralData generalData { get; set; }
        public List<Menus> menus { get; set; }

        public Root()
        {
            localhost_will_be_dynamic_domainname = new SiteModel();
            generalData = new GeneralData();
            menus = new List<Menus>();
            menus.Add(new Menus() { content = new List<ComponentsValues>() });
        }
    }

    public class SiteModel
    {
        public int pageId { get; set; }
        public ComponentsSections menu { get; set; }
        public ComponentsSections layout { get; set; }
        public ComponentsSections header { get; set; }
        public ComponentsSections footer { get; set; }
        public ComponentsSections content { get; set; }
        public string[] scripts { get; set; }
        public string[] styles { get; set; }

        public SiteModel()
        {
            styles = new string[]{};
            scripts = new string[]{};

            ComponentsSections m = new ComponentsSections();
            m.name = "page-scroll-menu";
            menu = m;

            ComponentsSections l = new ComponentsSections();
            l.name = "page-scroll-layout";
            layout = l;

            ComponentsSections h = new ComponentsSections();
            h.name = "basic-header";
            header = h;

            ComponentsSections f = new ComponentsSections();
            f.name = "basic-footer";
            footer = f;

            ComponentsSections c = new ComponentsSections();
            c.name = "page-scroll-content";
            content = c;

        }
    }

    public class GeneralData
    {
        public string CompanyName { get; set; }
		public string CompanyAddress { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string MainLogo { get; set; }
        public string CompanyFacebook { get; set; }
        public string CompanyLinkedin { get; set; }
        public string CompanyInstagram { get; set; }
        public string CompanyTwitter { get; set; }
        public string CompanyDescription { get; set; }
        public List<FooterLink> FooterLinks { get; set; }
        public string[] ScriptHead { get; set; }
        public string[] ScriptBodyTop { get; set; }
        public string[] ScriptBodyBottom { get; set; }
    }

    public class Menus
    {
        public string name { get; set; }
        public string subname { get; set; }
        //public string Name { get; set; }
        public string path { get; set; }
        public bool navigationHide { get; set; }
        public string menuIcon { get; set; }
        public string menuImg { get; set; }
        public string menuMp4 { get; set; }
        public string menuWebm { get; set; }
        public IEnumerable<Menus> subMenu { get; set; }
        public string metaTitle { get; set; } 
        public string metaDescription { get; set; } 
        public string metaKeywords { get; set; }
        public string metaImage { get; set; }
        public string favicon { get; set; }
        public List<ComponentsValues> content { get; set; }

        public Menus()
        {   
            name = null;
            subname = null;
            path = null;
            navigationHide = false;
            menuIcon = null;
            menuMp4 = null;
            menuWebm = null;
            subMenu = null;
            content = new List<ComponentsValues>();
            metaTitle = null;
            metaKeywords = null;
            metaDescription = null;
            metaImage = null;
            favicon = null;
        }

    }

    public class ComponentsSections
    {
        public string name { get; set; }
        public List<ComponentsValues> content { get; set; }
    }

    public class FooterLink
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }
    }

    public class ComponentsValues
    {
        public string name { get; set; }
        public string key { get; set; }
        public int nodeid { get; set; }
    }

    public class Banner : ComponentsValues
    {
        public string src { get; set; }
        public string mp4Url { get; set; }
        public string oggUrl { get; set; }
        public bool isAutoplay { get; set; }
        public bool isLoop { get; set; }
        public bool showControls { get; set; }
        public bool isMuted { get; set; }
        public string overlayMarkup { get; set; }
        public string bgColor { get; set; }
        public string posterSrc { get; set; }
        public bool displayOverlay { get; set; }
        public bool fullOverlay { get; set; }
    }

    public class Video : ComponentsValues
    {
        public string mp4Url { get; set; }
        public string oggUrl { get; set; }
        public bool isAutoplay { get; set; }
        public bool isLoop { get; set; }
        public bool isMuted { get; set; }
        public bool showControls { get; set; }
        public string src { get; set; }

    }

    public class Image : ComponentsValues
    {
        public string src { get; set; }
        public string alt { get; set; }
    }

    public class Text : ComponentsValues
    {
        public string markup { get; set; }
    }

    public class Teaser : ComponentsValues
    {
        public string markup { get; set; }
        public string src { get; set; }
        public bool sortingLeftRight { get; set; }
    }

    public class Slider : ComponentsValues
    {
        public string[] images { get; set; }
        public bool displayArrows { get; set; }
        public bool displayDots { get; set; }
    }

    public class Statment : ComponentsValues
    {
        public string heading { get; set; }
        public bool textCenter { get; set; }
        public List<StatmentChild> elements { get; set; }
    }

    public class StatmentChild
    {
        public string alias { get; set; }
        public string text { get; set; }
        public string iconClass { get; set; }
    }


    public class Testimonials : ComponentsValues
    {
        public bool designShuffle { get; set; }
        public List<TestimonialChild> elements { get; set; }
    }

    public class TestimonialChild
    {
        public string image { get; set; }
        public string text { get; set; }
        public string title { get; set; }
        public string name { get; set; }
    }

}
