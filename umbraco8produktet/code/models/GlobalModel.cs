﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace code.models
{
    public class GlobalModel
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }

        public string MainLogo { get; set; }

        public string CompanyFacebook { get; set; }
        public string CompanyLinkedin { get; set; }
        public string CompanyInstagram { get; set; }
        public string CompanyTwitter { get; set; }

        public string ScriptHead { get; set; }
        public string ScriptBodyTop { get; set; }
        public string ScriptBodyBottom { get; set; }


        public GlobalModel()
        {
        }

        public GlobalModel(IPublishedContent content)
        {

            var rootNode = content.AncestorOrSelf(1);

            CompanyName =
            CompanyAddress =
            CompanyAddress2 =
            CompanyPhone =
            CompanyEmail =

            MainLogo =

            CompanyFacebook =
            CompanyLinkedin =
            CompanyInstagram =
            CompanyTwitter =

            ScriptHead =
            ScriptBodyTop =
            ScriptBodyBottom;
        }

    }
}