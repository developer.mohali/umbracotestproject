﻿using System;
using Newtonsoft.Json;

namespace code.models
{
    public class MasterModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("updated")]
        public DateTime Updated { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("metaDescription")]
        public string MetaDescription { get; set; }

        [JsonProperty("metaKeywords")]
        public string MetaKeywords { get; set; }

        [JsonProperty("favicon")]
        public string Favicon { get; set; }

        [JsonProperty("domaine")]
        public object Domaine { get; set; }

        [JsonProperty("banner")]
        public string Banner { get; set; }

        [JsonProperty("usejsongrid")]
        public bool UseJsonGrid { get; set; }

        [JsonProperty("bodyContentGridJson")]
        public object bodyContentGridJson { get; set; }

        [JsonProperty("overskrift")]
        public object Overskrift { get; set; }

        [JsonProperty("overskriftUnder")]
        public object OverskriftUnder { get; set; }

        [JsonProperty("broedtekst")]
        public object Broedtekst { get; set; }

    }
}