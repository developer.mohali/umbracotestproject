﻿using System.Collections.Generic;
using code.models.helper;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace code.models
{
    public class FrontpageModel : MasterModel
    {
        /** not in use **/
        [JsonProperty("contentImages")]
        public IEnumerable<ImageModel> ContentImages { get; set; }

        [JsonProperty("contentBody")]
        public string ContentBody { get; set; }

        [JsonProperty("bodyContentGridJson")]
        public object bodyContentGridJson { get; set; } 

        public static FrontpageModel GetFromContent(IPublishedContent a)
        {
            return new FrontpageModel
            {

                //https://our.umbraco.org/documentation/reference/querying/ipublishedcontent/Properties
                Id = a.Id,
                Name = a.Name,
                ContentImages = ImageModel.GetImages(a, "contentImages", 640, 480),
                ContentBody = a.GetPropertyValue<string>("contentBody"),
                bodyContentGridJson = a.GetPropertyValue<object>("bodyContentGrid"),        
                Created = a.CreateDate,
                Updated = a.UpdateDate,
                Title = a.GetPropertyValue<string>("title"),
                MetaDescription = a.GetPropertyValue<string>("metaDescription")
                
            };        
        }
    }
}