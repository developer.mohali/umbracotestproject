﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using code.models.helper;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Services;
using umbraco.MacroEngines;

namespace code.models
{
    public class SubpageModel_Standard : MasterModel
    {

        //private UmbracoHelper _helper = new UmbracoHelper(UmbracoContext.Current);
        private static readonly string DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port); //HttpContext.Current.Request.Url.Host


        public static SubpageModel_Standard GetFromContent(IPublishedContent a)
        {
            var port = "";
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.Port.ToString()))
            {
                port = ":" + HttpContext.Current.Request.Url.Port.ToString();
            }
            var d = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + port;

            //Banner
            string bannerhtml = "";
            if (!string.IsNullOrEmpty(a.GetPropertyValue<string>("banner")))
            {
                bannerhtml = buildBannerHtml(a.GetPropertyValue<string>("banner"));
            } 

            return new SubpageModel_Standard
            {
                Id = a.Id,
                Name = a.Name,          
                Created = a.CreateDate,
                Updated = a.UpdateDate,
                Title = a.GetPropertyValue<string>("title"),
                MetaDescription = a.GetPropertyValue<string>("metaDescription"),
                MetaKeywords = a.GetPropertyValue<string>("metaKeywords"),
                Favicon = Getimage(a.GetPropertyValue<string>("favicon", recurse: true)).CroppedUrl,
                Domaine = d,
                UseJsonGrid = a.GetPropertyValue<bool>("benytAvanceretIndhold"),
                Overskrift = a.GetPropertyValue<string>("overskrift"),
                OverskriftUnder = a.GetPropertyValue<string>("overskriftUnder"),
                Broedtekst = a.GetPropertyValue<string>("broedtekst"),
                Banner = bannerhtml
            };
        }

        static string buildBannerHtml(string sliderids) {
            string i = sliderids;
            string html = "<div class='container-full bannerslick'>";

            foreach (var nodeid in i.Split(','))
            {
                dynamic node = new DynamicNode(nodeid);
  
                  html += "<div style='background-color:red;'>";
                  html += "<div class='slick-text'>";
                  if (!string.IsNullOrEmpty(node.title)) {
                    html += "<h1>" + node.title + "</h1>";
                  }

                  html += node.text;
                  html += "</div> ";

                  if (!string.IsNullOrEmpty(node.image)) {
                      html += "<img src='" + Getimage(node.image.ToString(), 1400, 320).CroppedUrl + "'alt='" + Getimage(node.image.ToString()).AltText + "' />";
                  }
                  html += "</div> ";
            }

            html += "</div>";

            return html;
        }

        static ImageModel Getimage(string imgid = "1", int width = 700, int height = 400)
        {
            var _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            if (imgid != "1")
            {
                var mediaItem = _umbracoHelper.TypedMedia(imgid);
                return new ImageModel
                {
                    Id = mediaItem.Id,
                    Url = DomainName + mediaItem.Url,
                    CroppedUrl = DomainName + mediaItem.GetCropUrl(width, height, preferFocalPoint: true),
                    AltText = mediaItem.HasValue("alttext") ? mediaItem.GetPropertyValue<string>("alttext") : ""
                };

            }
            else
            {
                return null;
            }
        }
    }
}
