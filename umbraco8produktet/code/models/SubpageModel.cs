﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using code.models.helper;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Services;
using umbraco.MacroEngines;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Archetype.Models;
using Umbraco.Core.Logging;
using System.Net;
using Umbraco.Core.Models.PublishedContent;

namespace code.models
{
    public class SubpageModel : MasterModel
    {
        //private UmbracoHelper _helper = new UmbracoHelper(UmbracoContext.Current);
        private static readonly string DomainName = string.Format("http://{0}", HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port); 
        
        public static SubpageModel GetFromContent(IPublishedContent a)
        {
            var port = "";
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.Port.ToString()))
            {
                port = ":" + HttpContext.Current.Request.Url.Port.ToString();
            }
            var d = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + port;

            //Banner
            string bannerhtml = "";
            if (!string.IsNullOrEmpty(a.GetPropertyValue<string>("banner")))
            {
                bannerhtml = buildSlider(a.GetPropertyValue<string>("banner")); // buildBannerHtml(a.GetPropertyValue<string>("banner"));
            } 

            object contentjson = new object();
            string overskrift = "";
            string overskriftunder = "";
            string brodtext = "";
            if (a.GetPropertyValue<bool>("benytAvanceretIndhold"))
            {
                contentjson = a.GetPropertyValue<object>("bodyContentGrid");
                if (!string.IsNullOrEmpty(contentjson.ToString()))
                {
                    var propsToMask = new HashSet<string>(new[] { "Image", "formularFields", "listitems", "SliderHtml" });
                    var jObj = JObject.Parse(contentjson.ToString().Replace("url(/", "url(" + d + "/").Replace("/media/", d + "/media/"));

                    foreach (var p in jObj.Descendants()
                                         .OfType<JProperty>()
                                         .Where(p => propsToMask.Contains(p.Name)))
                    {
                        if (p.Name == "Image")
                        {
                            p.Value = JToken.FromObject(Getimage(p.Value.ToString()));                        
                        }
                        else if (p.Name == "formularFields")
                        {
                            p.Value = JToken.FromObject(Getform(p.Value.ToString()));  
                        }
                        else if (p.Name == "listitems")
                        {
                            var settings = JToken.FromObject(p.Parent.Ancestors().FirstOrDefault().Children().FirstOrDefault());
                            int cardCount = 3;
                            try
                            {
                                cardCount = Convert.ToInt32(settings["cardcount"]);
                            }
                            catch (Exception)
                            {
                               
                            }
                           
                            p.Value = JToken.FromObject(buildListItems(p.Value.ToString(), cardCount));    
                        }
                        else if (p.Name == "SliderHtml")
                        {
                            var settings = JToken.FromObject(p.Parent.Ancestors().FirstOrDefault().Children().FirstOrDefault());
                            p.Value = JToken.FromObject(buildSlider(p.Value.ToString(), settings["sliderHeight"].ToString(), settings["speed"].ToString(), settings["slidesToShow"].ToString(), Convert.ToBoolean(settings["showarrows"]), Convert.ToBoolean(settings["showdots"])));
                        }
                    }

                    contentjson = jObj;
                }

            }
            else
            {
                overskrift = a.GetPropertyValue<string>("overskrift");
                overskriftunder = a.GetPropertyValue<string>("overskriftUnder");
                brodtext = a.GetPropertyValue<string>("broedtekst");
            }

            var favicon = "";
            if (a.AncestorsOrSelf("siteSettings").Last().HasValue("favicon"))
            {
                favicon = Getimage(a.AncestorsOrSelf("siteSettings").Last().GetPropertyValue<string>("favicon", true), 16, 16).Url;
            }

            return new SubpageModel
            {
                Id = a.Id,
                Name = a.Name,              
                Created = a.CreateDate,
                Updated = a.UpdateDate,
                Title = a.GetPropertyValue<string>("title"),
                MetaDescription = a.GetPropertyValue<string>("metaDescription"),
                MetaKeywords = a.GetPropertyValue<string>("metaKeywords"),
                Favicon = favicon,
                Domaine = d,
                Banner = bannerhtml,
                UseJsonGrid = a.GetPropertyValue<bool>("benytAvanceretIndhold"),
                bodyContentGridJson = contentjson, 
                Overskrift = overskrift,
                OverskriftUnder = overskriftunder,
                Broedtekst = brodtext
            };
        }

        static string buildSlider(string sliderids, 
                                  string height = "340", 
                                  string speed = "400", 
                                  string slidesToShow = "1",
                                  bool showarrows = false,
                                  bool showdots = false){
            
            string html = String.Format("<div class='container-full' data-height='{0}' data-speed='{1}' data-slidestoshow='{2}' data-showarrows='{3}' data-showdots='{4}'>", height, speed, slidesToShow, showarrows, showdots);

            foreach (var nodeid in sliderids.Split(','))
            {
                dynamic node = new DynamicNode(nodeid);

                  html += "<div style='background-color: " + node.backgroundColor + ";'>";
                  html += "<div class='slick-text'>";
                  html += "<h1>" + node.title + "</h1>";
                  html += node.text;
                  html += "</div>";
                  if (node.HasValue("image"))
                  {
                      ImageModel img = Getimage(node.image);
                      html += "<img src='" + img.Url + "'alt='" + img.AltText + "' />";
                  }

                  html += "</div> ";
            }

            html += "</div>";

            return html;
        }

        static string buildListItems(string itemids, int cardCount = 3)
        {
            cardCount = 12 / cardCount;

            string html = "<div>";
            foreach (var nodeid in itemids.Split(','))
            {
                dynamic node = new DynamicNode(nodeid);



                html += "<div class='col s12 m" + cardCount.ToString() + "'>";
                html += "<div class='card'>";
                if (!string.IsNullOrEmpty(node.image))
                {
                    html += "<div class='card-image'>";
                    html += "<img src='" + Getimage(node.image.ToString()).CroppedUrl + "' alt='" + Getimage(node.image.ToString()).AltText + "' />";
                    if (!string.IsNullOrEmpty(node.imageTitle))
                    {
                        html += "<span class='card-title'>" + node.imageTitle + "</span>";
                    }
                    html += "</div>";
                }
                 // card title
                 html += "<div class='card-content'>";
                 if (!string.IsNullOrEmpty(node.title)) {
                     html += "<span class='card-title'>" + node.title + "</span>";
                 }
                 //card text
                 html += node.text;
               
                 html += "</div>";
                
                 //card link
                 if (node.HasValue("link"))
                 {
                    var nodeLink = (string)node.link;
                    var token = JToken.Parse(nodeLink);
                    var href = token["url"];
                    if (node.HasValue("link.hashtarget")) {
                        href = "#" + token["link.hashtarget"];
                    }
                    html += "<div class='card-action'>";
                    html += "<a href='" + href + "' target='" + token["target"] + "''>" + token["name"] + "</a>";
                    html += "</div>";
                 }

                html += "</div>";
                html += "</div>";
            }

            html += "</div>";

            return html;
        }

        static string buildBannerHtml(string bannerids)
        {
            string i = bannerids;
            string html = "<div class='container-full bannerslick'>";

            foreach (var nodeid in i.Split(','))
            {
                dynamic node = new DynamicNode(nodeid);

                html += "<div style='background-color:red;'>";
                html += "<div class='slick-text'>";
                if (!string.IsNullOrEmpty(node.title))
                {
                    html += "<h1>" + node.title + "</h1>";
                }

                html += node.text;
                html += "</div> ";

                if (!string.IsNullOrEmpty(node.image))
                {
                    html += "<img src='" + Getimage(node.image.ToString(), 1400, 320).CroppedUrl + "'alt='" + Getimage(node.image.ToString()).AltText + "' />";
                }
                html += "</div> ";
            }

            html += "</div>";

            return html;
        }

        static ImageModel Getimage(string imgid = "1", int width = 700, int height = 400)
        {
            var _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            if (imgid != "1")
            {
                var mediaItem = _umbracoHelper.TypedMedia(imgid);
                return new ImageModel
                {
                    Id = mediaItem.Id,
                    Url = DomainName + mediaItem.Url,
                    CroppedUrl = DomainName + mediaItem.GetCropUrl(width, height, preferFocalPoint: true),
                    AltText = mediaItem.HasValue("alttext") ? mediaItem.GetPropertyValue<string>("alttext") : ""
                };

            }
            else
            {
                return null;
            }
        }

        static List<FormModel> Getform(string formid = "1")
        {

            try
            {

                var result = new List<FormModel>();
                List<dynamic> elm = new List<dynamic>();

                var n = new umbraco.NodeFactory.Node(Convert.ToInt32(formid));
                var fields = JsonConvert.DeserializeObject<ArchetypeModel>(n.GetProperty("fields").Value);
                int count = 0;
                foreach (var f in fields.Fieldsets.Where(x => x != null && x.Properties.Any()))
                {
                    elm.Add(new
                    {

                        id = "form_" + formid + "field_" + count,
                        //not sure what model is used for.....
                        model = "",
                        type = "input",
                        label = f.GetValue("label"),
                        //ratio, radiobuttonlist, email
                        subType = "text",
                        cssclass = new List<string> { "input-field", "col", "s6" },
                        //style =  {'font-weight': 'bold'},
                        wrap = new List<string> { "" },  //row
                        placeholder = f.GetValue("placeholderText"),
                        required = f.GetValue("required") == "1",
                        maxlength = f.GetValue("maxLength"),
                        minlength = f.GetValue("minLength"),

                    });
                    count++;
                }

                result.Add(new FormModel
                {
                    formId = n.Id,
                    errorMessage = n.GetProperty("errorMessage").Value,
                    successMessage = n.GetProperty("thanksMessage").Value,
                    buttonText = n.GetProperty("buttonText").Value,
                    elements = elm 
                });

                return result; 
            }
            catch (Exception ex)
            {
                LogHelper.Info(typeof(SubpageModel), String.Format("Der skete en fejl: {0}", ex.Message));
                return null;
            }
        }
    
    }
}
