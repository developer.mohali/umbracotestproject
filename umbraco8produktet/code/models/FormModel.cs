﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace code.models
{
    public class FormModel
    {
        [JsonProperty("formId")]
        public int formId { get; set; }

        [JsonProperty("errorMessage")]
        public string errorMessage { get; set; }

        [JsonProperty("successMessage")]
        public string successMessage { get; set; }

        [JsonProperty("buttonText")]
        public string buttonText { get; set; }

        [JsonProperty("elements")]
        public object elements { get; set; }
  
    }
}
