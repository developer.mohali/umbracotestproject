﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace code.models
{
    public class NodesModel
    {
        [JsonProperty("id")]        
        public string id { get; set; }

        [JsonProperty("parentId")]
        public string parentId { get; set; }

        [JsonProperty("level")]
        public string level { get; set; }
        
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("contentTypeAlias")]
        public string contentTypeAlias { get; set; }

        [JsonProperty("path")]
        public string path { get; set; }

        [JsonProperty("component")]
        public string component { get; set; }
        
        [JsonProperty("navigationHide")] 
        public bool navigationHide { get; set; }

        [JsonProperty("menuIcon")]
        public string menuIcon { get; set; }

        public NodesModel()
        {
          id = "";
          parentId = "";
          level = "";
          name = "";
          contentTypeAlias = "";    
          path = "";
          component = "";
          navigationHide = false;
          menuIcon = "";
        }
  
    }

 
}
