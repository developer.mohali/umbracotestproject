﻿using System;
using System.Net;
using System.Web;
using Skybrud.WebApi.Json;
using Umbraco.Web.WebApi;
using Umbraco.Core.Models;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace code.controllers
{
    [JsonOnlyConfiguration]
    public class FormApiController : UmbracoApiController
    {
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public string SaveFormRequest(int formid = 1132, string formcontent = "<p>Please make template for email in Umbraco backend</p>", string formcontentjson = "{ 'formjson':'Empty' }")
        {

            // HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            try
            {

                var formnode = Services.ContentService.GetById(formid);

                var emailbody = "";
                if (formcontent == "<p>Please make template for email in Umbraco backend</p>")
                {
                    var dynJson = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(formcontentjson)); //JsonConvert.DeserializeObject(HttpUtility.UrlEncode(formcontentjson));
                    var jObj = JObject.Parse(dynJson.ToString());
                    emailbody = "<p>";
                    var s = "";
                    foreach (var item in jObj)
                    {
                        s += item.Key + ": " + item.Value + "<br/>";
                    }
                    emailbody += formnode.GetValue<string>("template").Replace("{formdata}", s) +"</p>";

                }
                else {
                    emailbody = formnode.GetValue<string>("template").Replace("{formdata}", formcontent);
                }

                bool saveOk = SaveFormData(formid, formcontent, formnode.Name);
                string mailsendinfo = SendEmail(formnode.GetValue<string>("mailTo"), formnode.GetValue<string>("mailFrom"), formnode.GetValue<string>("mailToCC"), "Website - " + formnode.Name, emailbody);

                //return "Fejlbesked på mail: " + mailsendinfo + " ----- Gemt i Umbraco backend: " + saveOk.ToString();
                return "message sent";
            }
            catch (Exception ex)
            {
                return ex.ToString(); // "Der er desværre sket en fejl. Vi håber I vil kontakte os på anden vis";
            }

        }
     
        private bool SaveFormData(int nodeId, string formData, string nodeName)
        {
            try
            {
               string submitDate = DateTime.Now.ToString("MMMM dd, yyyy @ hh:mm:ss tt");
               nodeName = nodeName + " - " + submitDate;
               IContent submitForm = ApplicationContext.Services.ContentService.CreateContent(nodeName, nodeId, "FormEntry");
               submitForm.SetValue("submitDate", submitDate);
               submitForm.SetValue("formData", formData);
               
               ApplicationContext.Services.ContentService.Save(submitForm, 0, false);
               ApplicationContext.Services.ContentService.PublishWithStatus(submitForm);

               return true;
            }
            catch
            {
               return false;
            }
        }

        private string SendEmail(string toEmail, string fromMail, string ccEmail, string emailSubject, string emailBody)
        {
            try
            {
                var mail = new MailMessage();
                var client = new SmtpClient("mail.smtp2go.com", 2525)
                {
                    Credentials = new NetworkCredential("formular@dynamikfabrikken.com", "YnhxdXFiaWtvcjAw"),
                    EnableSsl = true
                };

                mail.From = new MailAddress(fromMail);
                if (!string.IsNullOrWhiteSpace(toEmail))
                {
                    foreach (string to in toEmail.Split(';'))
                    {
                        mail.To.Add(new MailAddress(to));
                    }
                }
                if (!string.IsNullOrWhiteSpace(ccEmail))
                {
                    foreach (string cc in ccEmail.Split(';'))
                    {
                        mail.CC.Add(new MailAddress(cc));
                    }
                }
                mail.Subject = emailSubject;
                var plainView = AlternateView.CreateAlternateViewFromString(Regex.Replace(emailBody, @"<[^>]+>|&nbsp;", "").Trim(), null, "text/plain");
                var htmlView = AlternateView.CreateAlternateViewFromString(emailBody, null, "text/html");
                mail.AlternateViews.Add(plainView);
                mail.AlternateViews.Add(htmlView);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return string.Empty;
        }

    }
}


