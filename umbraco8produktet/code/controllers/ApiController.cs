﻿using code.helpers;
using Skybrud.WebApi.Json;
using Skybrud.WebApi.Json.Meta;
using System;
using System.Net;
using Umbraco.Web.WebApi;
using System.Collections.Generic;
using code.models;
using static code.helpers.NodeIdHelper;

namespace code.controllers
{

    [JsonOnlyConfiguration]
    public class ApiController : UmbracoApiController
    {
  
        // sample: /umbraco/api/Api/GetSite?domain=dynamikfabrikken.dk&noCache=true
        public object GetSite(string domain = "localhost", bool noCache = false)
        {
            var cacheKey = "GetSite_" + domain;
            var dataFromCache = MemoryCacher.GetValue(cacheKey);
            //if (dataFromCache != null && !noCache)
            //{
            //    return Request.CreateResponse(JsonMetaResponse.GetSuccess(dataFromCache));
            //}

            try
            {
                //get customers website settings
                CustomerInfo customerInfo = GetCustomerInfoByDomain(domain);

                //get nodedata
                var node = Umbraco.Content(customerInfo.rootNodeId);
                var r = new Dictionary<string, object>();
                

                //Mapping things up for json feed

                var siteModel = new SiteModel();
                //Site components info
                siteModel.pageId = node.Id;
                siteModel.menu = customerInfo.menu;
                siteModel.layout = customerInfo.layout;
                siteModel.header = customerInfo.header;
                siteModel.footer = customerInfo.footer;
                siteModel.content = customerInfo.content;
                siteModel.styles = customerInfo.Styles; 
                siteModel.scripts = customerInfo.Scripts;

                r.Add("mainComponents", siteModel);

                //Generaldata 
                var generalData = GeneralDataHelper.GetGeneralData(node);
          
                //sites additional/own scripts to add
                generalData.ScriptHead = customerInfo.ScriptHead;
                generalData.ScriptBodyTop = customerInfo.ScriptBodyTop;
                generalData.ScriptBodyBottom = customerInfo.ScriptBodyBottom;
                r.Add("generalData", generalData);

                //Menu structure and content to add
                var menus = new List<Menus>();
                menus.Add(MenuTreeHelper.getMenuItem(node, false));
                foreach (var publishedContent in node.Children)
                {
                    menus.Add(MenuTreeHelper.getMenuItem(publishedContent, true));
                }
                r.Add("menus", menus);

                MemoryCacher.Add(cacheKey, r, new DateTimeOffset(DateTime.Now.AddHours(1)));
                return Ok(r);
            }
            catch (Exception ex)
            {
                Logger.Info(typeof(ApiController), String.Format("Api issue: {0}, request: {1}", ex.Message, cacheKey));

                // 500 error
                return Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, "Error from server: " + ex.Message));
            }
        }


        // sample: /umbraco/api/Api/GetClearSiteCache?domain=dynamikfabrikken.dk
        public string GetClearSiteCache(string domain = "ThereWillBeNoDomainNameLikeThis")
        {
            var cacheKey = "GetSite_" + domain;
        
            if (domain == "ThereWillBeNoDomainNameLikeThis")
            {
                return "Error has happend! Please add: ?domain=[relatedDomainName]";
            }
        
            try
            {
                MemoryCacher.Delete(cacheKey);
                GetSite(domain);
                return "Cache cleared - key: " + cacheKey;
            }
            catch (Exception ex)
            {
                Logger.Info(typeof(ApiController), String.Format("Api issue: {0}, request: {1}", ex.Message, cacheKey));
                return "Error has happend! Pls. check log";
            }
            
        }

        // sample: /umbraco/api/Api/GetGrid?nodeid=1104&key=f76e1f33-5e15-41a2-96aa-bb8a8554e545&noCache=true
        //public object GetGrid(int nodeid = 1152, int key = 0, bool noCache = false)
        //{
        //    var cacheKey = "GetSite_" + nodeid.ToString();
        //    var dataFromCache = MemoryCacher.GetValue(cacheKey);
        //    if (dataFromCache != null && !noCache)
        //    {
        //        return Request.CreateResponse(JsonMetaResponse.GetSuccess(dataFromCache));
        //    }

        //    try
        //    {
        //        var node = Services.ContentService.GetById(nodeid);
        //        var gridmarkup = GridHtmlHelper.GetGridHtml(node.GetValue("bodyText").ToString());

        //        MemoryCacher.Add(cacheKey, gridmarkup, new DateTimeOffset(DateTime.Now.AddHours(1)));
        //        return Request.CreateResponse(JsonMetaResponse.GetSuccess(gridmarkup));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Info(typeof(ApiController), String.Format("Api issue: {0}, request: {1}", ex.Message, cacheKey));
        //        // 500 error
        //        return Request.CreateResponse(JsonMetaResponse.GetError(HttpStatusCode.InternalServerError, "Error from server: " + ex.Message));
        //    }
        //}

    }
}


