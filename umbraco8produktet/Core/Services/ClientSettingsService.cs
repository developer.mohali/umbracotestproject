﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Umbraco.Core.Cache;

namespace Core.Services
{
    public class ClientSettingsService : IClientSettingsService
    {
        private readonly IAppPolicyCache _runtimeCache;

        public ClientSettingsService(AppCaches appCaches)
        {
            // Again we can just grab RuntimeCache from AppCaches.
            _runtimeCache = appCaches.RuntimeCache;
        }

        public List<ClientSettings> GetClientJson()
        {
            string cacheKey = "clientSettings";
            var timeout = new TimeSpan(0, 1, 0, 0);

            return _runtimeCache.GetCacheItem(cacheKey, () =>
            {
                string customerJson = string.Empty;
                customerJson = File.ReadAllText(HttpContext.Current.Server.MapPath("~/clientSettings.json"), Encoding.Default);
                List<ClientSettings> customerlist = JsonConvert.DeserializeObject<List<ClientSettings>>(customerJson);

                return customerlist;
            }, timeout);
        }

        public ClientSettings GetClientSettings()
        {
            List<ClientSettings> customerlist = GetClientJson();

            //var c = customerlist.FirstOrDefault(x => x.RootnodeId == Id) ??
            //        customerlist.FirstOrDefault(x => x.RootnodeId == 1147);            
            
            var c = customerlist.FirstOrDefault();

            return c;
        }
    }

    public interface IClientSettingsService
    {
        List<ClientSettings> GetClientJson();
        ClientSettings GetClientSettings();
    }

    public class ClientSettings
    {
        public string Site { get; set; }
        public int RootnodeId { get; set; }
        public bool HasSnipCart { get; set; }
        public int DymmyImageId { get; set; }
        public string[] Styles { get; set; }
        public string[] Scripts { get; set; }
        public string[] ContentBlocks { get; set; }

}
}