using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Management.Common;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Web.Administration;
using System.Linq;
using Microsoft.SqlServer.Management.Smo;
using System.IO.Compression;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Core.Models.Membership;
using Umbraco.Web.Security.Providers;
using Umbraco.Web.Security;
using System.Web.Security;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Configuration;

namespace Core.Controllers
{
    public class FormularController : UmbracoApiController
    {
        string sitename = "", hostname = "", UserName = "", UserEmail = "", Password = "", URL = "";

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string sumbitFormular([FromBody] string model)
        {
            string r = string.Empty;

            NameValueCollection collection = HttpUtility.ParseQueryString(model);

            //I am trying to get dictionary items for string[] fieldsToInclude. Somehow it is not working
            // Will we need language (var lang) ?
            var lang = HttpContext.Current.Server.UrlDecode(collection["lang"]);
            var email = Umbraco.GetDictionaryValue("E-mail");
            var name = Umbraco.GetDictionaryValue("Name");
            var message = Umbraco.GetDictionaryValue("Message");

            string[] fieldsToInclude = { email, name, message };

            foreach (string value in fieldsToInclude)
            {
                if (!string.IsNullOrEmpty(collection[value.ToString()]))
                {
                    r += "<p>" + value + ": " + HttpContext.Current.Server.UrlDecode(collection[value.ToString()]) +
                         "</p>";
                }
            }

            string FilePath =
                HttpContext.Current.Server.MapPath(HttpContext.Current.Server.UrlDecode(collection["emailtemplate"]));
            StreamReader str = new StreamReader(FilePath);
            string MailText = str.ReadToEnd();
            //Regex yourRegex = new Regex(@"\@{([^\}]+)\}");
            //MailText = yourRegex.Replace(MailText, "anyReplacement");
            str.Close();
            r = MailText.Replace("{{dynamictext}}", r)
                .Replace("{{emailsignatur}}", HttpContext.Current.Server.UrlDecode(collection["emailsignatur"]))
                .Replace("{{introtext}}", HttpContext.Current.Server.UrlDecode(collection["emailintro"]));

            SendEmail(HttpContext.Current.Server.UrlDecode(collection["emailto"]),
                HttpContext.Current.Server.UrlDecode(collection["emailfrom"]), "",
                HttpContext.Current.Server.UrlDecode(collection["emailSubject"]), r);

            if (!string.IsNullOrEmpty(HttpContext.Current.Server.UrlDecode(collection["E-mail"])))
            {
                SendEmail(HttpContext.Current.Server.UrlDecode(collection["E-mail"]),
                    HttpContext.Current.Server.UrlDecode(collection["emailfrom"]), "",
                    HttpContext.Current.Server.UrlDecode(collection["emailSubject"]), r);
            }

            return r;

        }


        // Submit for Create and Host Site in IIS.
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string sumbitCloneSiteFormular([FromBody] string model)
        {
            string r = string.Empty;
            try
            {

                NameValueCollection collection = HttpUtility.ParseQueryString(model);

                sitename = HttpContext.Current.Server.UrlDecode(collection["SiteName"]);
                hostname = HttpContext.Current.Server.UrlDecode(collection["HostName"]);
                UserName = HttpContext.Current.Server.UrlDecode(collection["UserName"]);
                UserEmail = HttpContext.Current.Server.UrlDecode(collection["UserEmail"]);

                //File.AppendAllText(path, "SiteName:" + sitename + Environment.NewLine);
                //File.AppendAllText(path, "HostName:" + hostname + Environment.NewLine);
                //File.AppendAllText(path, "UserName:" + UserName + Environment.NewLine);
                //File.AppendAllText(path, "UserEmail:" + UserEmail + Environment.NewLine);

                // This path getting from web.config file
                string DestinationPath = ConfigurationManager.AppSettings.Get("SiteDestinationPath");

                //File.AppendAllText(path, "DestinationPath:" + DestinationPath + Environment.NewLine);
                try
                {
                    if (!Directory.Exists(DestinationPath))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(DestinationPath);
                    }

                    string SourcePath = "";
                    String FilePath;
                    FilePath = AppDomain.CurrentDomain.BaseDirectory;

                    // Create Source Path 
                    SourcePath = Path.Combine(FilePath, ConfigurationManager.AppSettings.Get("SourceZipFolderName"));
                    //File.AppendAllText(path, "SourcePath:" + SourcePath + Environment.NewLine);
                    //sw.WriteLine("SourcePath:" + SourcePath);
                    string fileName = "";
                    var files = Directory.EnumerateFiles(SourcePath, "*.*", SearchOption.AllDirectories)
                    .Where(s => s.EndsWith(".zip"));
                    fileName = Path.GetFileName(files.FirstOrDefault());
                    string file = Path.GetFileNameWithoutExtension(files.FirstOrDefault());

                    String ZipPath = Path.Combine(SourcePath, fileName);
                    //File.AppendAllText(path, "ZipPath:" + ZipPath + Environment.NewLine);
                    //sw.WriteLine("ZipPath:" + ZipPath);
                    string destinationPath = Path.Combine(DestinationPath, file);
                    if (Directory.Exists(destinationPath))
                    {
                        DeleteDirectory(destinationPath);
                    }
                    ZipFile.ExtractToDirectory(files.FirstOrDefault(), DestinationPath);
                    DestinationPath = Path.Combine(DestinationPath, file);
                    //File.AppendAllText(path, "DestinationPath:" + DestinationPath + Environment.NewLine);
                    //sw.WriteLine("DestinationPath:" + DestinationPath);
                    try
                    {
                        string filePath = FilePath + @"bin\Exe\IISSetup.exe";
                        //File.AppendAllText(path, "filePath:" + filePath + Environment.NewLine);
                        //sw.WriteLine("filePath:" + filePath);
                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        startInfo.UseShellExecute = false;
                        startInfo.FileName = FilePath + @"bin\Exe\IISSetup.exe";
                        startInfo.Verb = "runas";
                        startInfo.Arguments = DestinationPath + " " + sitename + " " + hostname;

                        Process process = System.Diagnostics.Process.Start(startInfo);
                        process.WaitForExit();
                        process.ExitCode.ToString();

                        RestoreDB(DestinationPath);
                        //File.AppendAllText(path, "Restore Database Success" + Environment.NewLine);
                        //sw.WriteLine("Restore Database Success");
                        URL = "http://localhost:80/";
                        string apiURL = URL + "Umbraco/Api/Formular/CreateUser";
                        string urlParameters = "?UserName=" + UserName + "&UserEmail=" + UserEmail;

                        // Call Creat user API
                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(apiURL);
                        // Add an Accept header for JSON format.
                        client.DefaultRequestHeaders.Accept.Add(
                                    new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string result = response.Content.ReadAsStringAsync().Result;
                            Password = JsonConvert.DeserializeObject<string>(result);
                        }
                        else
                        {
                            Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                        }
                        client.Dispose();
                        //File.AppendAllText(path, "User Created Successfully" + Environment.NewLine);
                        //sw.WriteLine("User Created Successfully");
                        //File.AppendAllText(path, "Password:" + Password + Environment.NewLine);
                        //sw.WriteLine("Password:" + Password);
                    }
                    catch (Exception ex)
                    {
                        return ex.Message;
                    }

                    r += "<p> Site Address For Local : " + URL + " </p>";
                    r += "<p> Site Address with HostName : http://" + hostname + "</p>";
                    r += "<p> UserName : " + UserName + "</p>";
                    r += "<p> UserEmail : " + UserEmail + "</p>";
                    r += "<p> Password : " + Password + "</p>";

                    string _FilePath =
                        HttpContext.Current.Server.MapPath(HttpContext.Current.Server.UrlDecode(collection["emailtemplate"]));
                    StreamReader str = new StreamReader(_FilePath);
                    string MailText = str.ReadToEnd();

                    str.Close();
                    r = MailText.Replace("{{dynamictext}}", r)
                        .Replace("{{emailsignatur}}", HttpContext.Current.Server.UrlDecode(collection["emailsignatur"]))
                        .Replace("{{introtext}}", HttpContext.Current.Server.UrlDecode(collection["emailintro"]));

                    // Send Email to user with details
                    SendEmail(UserEmail,
                        HttpContext.Current.Server.UrlDecode(collection["emailfrom"]), "",
                        HttpContext.Current.Server.UrlDecode(collection["emailSubject"]), r);
                }
                catch (Exception e)
                {
                    Console.WriteLine("The process failed: {0}", e.ToString());
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return r;
        }

        // This is Method for Delete directory with all sub folders and files.
        // Parameter = target_dir : pass directory path to delete 
        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                System.IO.File.SetAttributes(file, FileAttributes.Normal);
                System.IO.File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }

        // Copy Database from source to destination
        // Create New database
        public void RestoreDB(string path)
        {
            try
            {
                String FilePath;
                FilePath = AppDomain.CurrentDomain.BaseDirectory;
                // Create database source file 
                FilePath = Path.Combine(FilePath, ConfigurationManager.AppSettings.Get("SourceDatabaseFolderName"));
                var files = Directory.EnumerateFiles(FilePath, "*.*", SearchOption.AllDirectories)
                .Where(s => s.EndsWith(".sdf"));
                string fileName = Path.GetFileName(files.FirstOrDefault());
                //if (Path.GetExtension(fileName) == ".sdf")
                //{
                //    // Create Destination database path
                //    string sfile = Path.Combine(path, "App_Data");
                //    if (!Directory.Exists(sfile))
                //    {
                //        Directory.CreateDirectory(sfile);
                //    }
                //    DirectoryInfo dInfo = new DirectoryInfo(sfile);
                //    DirectorySecurity dSecurity = dInfo.GetAccessControl();
                //    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                //    dInfo.SetAccessControl(dSecurity);
                //    System.IO.File.Copy(files.FirstOrDefault(), Path.Combine(sfile, fileName), true);

                //    string webConfigPath = Path.Combine(path, "Web.Config");
                //    XmlDocument doc = new XmlDocument();

                //    FileSystemAccessRule iis_iusrs = new FileSystemAccessRule("IIS_IUSRS",
                //        FileSystemRights.FullControl,
                //        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                //        PropagationFlags.None,
                //        AccessControlType.Allow);
                //    FileSystemAccessRule networkService = new FileSystemAccessRule("NETWORK SERVICE",
                //        FileSystemRights.FullControl,
                //        InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                //        PropagationFlags.None,
                //        AccessControlType.Allow);

                //    dInfo = new DirectoryInfo(path);
                //    dSecurity = dInfo.GetAccessControl();
                //    dSecurity.AddAccessRule(iis_iusrs);
                //    dSecurity.AddAccessRule(networkService);
                //    dInfo.SetAccessControl(dSecurity);

                //    doc.Load(webConfigPath);
                //    XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", "umbracoDbDSN"));
                //    XmlNode node = list[0];

                //    string ConctionString = node.Attributes["connectionString"].Value;
                //    node.Attributes["connectionString"].Value = string.Format("Data Source=|DataDirectory|\\{0};Flush Interval=1;", fileName);

                //    dInfo = new DirectoryInfo(webConfigPath);
                //    dSecurity = dInfo.GetAccessControl();
                //    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                //    dInfo.SetAccessControl(dSecurity);

                //    doc.Save(webConfigPath);

                //}
                //else
                //{
                //    string serverName = "DESKTOP-2UP9E86\\SQLEXPRESS";
                //    string userName = "sa";
                //    string password = "Giriraj@1234";
                //    ServerConnection connection = new ServerConnection(serverName, userName, password);
                //    var sqlServer = new Server(connection);
                //    var rstDatabase = new Restore();
                //    var isExist = sqlServer.Databases.Contains("demo");
                //    if (!isExist)
                //    {
                //        rstDatabase.Database = "demo";
                //        rstDatabase.Action = RestoreActionType.Database;
                //        rstDatabase.Devices.AddDevice(FilePath + "\\cardb.bak", DeviceType.File);
                //        rstDatabase.ReplaceDatabase = true;

                //        //foreach (DataRow r in rstDatabase.ReadFileList(sqlServer).Rows)
                //        //{
                //        //    var relocateFile = new RelocateFile();

                //        //    relocateFile.LogicalFileName = r["LogicalName"].ToString();

                //        //    var physicalName = r["PhysicalName"].ToString();
                //        //    var path = System.IO.Path.GetDirectoryName(physicalName);
                //        //    var filename = System.IO.Path.GetFileName(physicalName);
                //        //    physicalName = System.IO.Path.Combine(path, string.Format("{0}_{1}", name, filename));

                //        //    relocateFile.PhysicalFileName = physicalName;

                //        //    rstDatabase.RelocateFiles.Add(relocateFile);
                //        //}

                //        rstDatabase.SqlRestore(sqlServer);

                //        connection.Disconnect();
                //    }
                //}
            }
            catch (Exception ex)
            {
                string exe = ex.StackTrace;
            }
        }

        // SEnd Email with template
        private string SendEmail(string toEmail, string fromMail, string ccEmail, string emailSubject, string emailBody)
        {
            try
            {
                var mail = new MailMessage();
                var client = new SmtpClient("mail.smtp2go.com", 2525)
                {
                    Credentials = new NetworkCredential("formular@dynamikfabrikken.com", "YnhxdXFiaWtvcjAw"),
                    EnableSsl = true
                };

                mail.From = new MailAddress(fromMail);
                if (!string.IsNullOrWhiteSpace(toEmail))
                {
                    foreach (string to in toEmail.Split(';'))
                    {
                        mail.To.Add(new MailAddress(to));
                    }
                }

                if (!string.IsNullOrWhiteSpace(ccEmail))
                {
                    foreach (string cc in ccEmail.Split(';'))
                    {
                        mail.CC.Add(new MailAddress(cc));
                    }
                }

                mail.Subject = emailSubject;
                var plainView =
                    AlternateView.CreateAlternateViewFromString(Regex.Replace(emailBody, @"<[^>]+>|&nbsp;", "").Trim(),
                        null, "text/plain");
                var htmlView = AlternateView.CreateAlternateViewFromString(emailBody, null, "text/html");
                mail.AlternateViews.Add(plainView);
                mail.AlternateViews.Add(htmlView);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return string.Empty;
        }


        // Create User in umbraco
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string CreateUser(string UserName, string UserEmail)
        {
            var us = Services.UserService;
            var user = us.CreateUserWithIdentity(UserEmail, UserEmail);
            // Get user group as IReadOnlyUserGroup
            var userGroup = Services.UserService.GetUserGroupByAlias("admin") as IReadOnlyUserGroup;
            // Add the userGroup to the newUser
            user.AddGroup(userGroup);
            // Set the user's Name
            user.Name = UserName;
            // Set the user's password
            string password = CreatePassword(8);            
            user.RawPasswordValue = (Membership.Providers["UsersMembershipProvider"] as UsersMembershipProvider).HashPasswordForStorage(password);
            us.Save(user);

            return password;
        }

        // Generate Password
        // Parameter = length : size of the password
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}
