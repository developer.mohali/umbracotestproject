﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web.Editors;
using Umbraco.Web.PublishedModels;


namespace Core.Controllers
{
	/// <summary>
	/// Controller allowing editing functionality of main Umbraco content
	/// </summary>
	public class UmbracoContentController : UmbracoAuthorizedJsonController
	{
		#region Read-Only Properties

		/// <summary>
		/// The root page for the Content section
		/// </summary>
		public IPublishedContent RootPage { get; }

		/// <summary>
		/// Property for exposing the Content Service instead of the (Umbraco's anti-)pattern singleton
		/// </summary>
		public IContentService ContentServiceProperty { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Basic controller constructor
		/// </summary>
		public UmbracoContentController()
		{
			RootPage = Umbraco.ContentAtRoot().Where(x => x.ContentType.Alias.Equals(Home.ModelTypeAlias)).FirstOrDefault();
			ContentServiceProperty = Services.ContentService;
		}

		#endregion

		#region Controller Actions

		/// <summary>
		/// Retrieves asynchronously the Home page
		/// </summary>
		/// <example>http://localhost/umbraco8produktet/Umbraco/Api/UmbracoContent/Get</example>
		/// <returns></returns>
		[HttpGet]
		public async Task<HomeResponse> Get()
		{
			var home = new Home(RootPage);
			var response = new HomeResponse(home);

			return response;
		}

		/// <summary>
		/// Updates the Home page content
		/// </summary>
		/// <example>
		/// http://localhost/umbraco8produktet/Umbraco/Api/UmbracoContent/Update
		/// {
		///	"Name": "NAME",
		///	"Address": "ADDRESS",
		///	"Phone": "111111111",
		///	"Email": "me@localhost",
		///	"CVR": "222222222"
		/// }
		/// </example>
		/// <param name="response">The POCO to populate with</param>
		/// <returns></returns>
		[HttpPost]
		public async Task<string> Update(HomeResponse response)
		{
			try
			{
				if (response == null) throw new NullReferenceException("No value passed in!");

				IContent content = ContentServiceProperty.GetById(RootPage.Id);

				// look ma - no magic strings! \o/
				content.SetValue(Home.GetModelPropertyType(d => d.BoCompanyName).Alias, response.Name);
				content.SetValue(Home.GetModelPropertyType(d => d.BoCompanyAddress).Alias, response.Address);
				content.SetValue(Home.GetModelPropertyType(d => d.BoCompanyPhone).Alias, response.Phone);
				content.SetValue(Home.GetModelPropertyType(d => d.BoCompanyEmail).Alias, response.Email);
				content.SetValue(Home.GetModelPropertyType(d => d.BoCompanyCvr).Alias, response.CVR);

				ContentServiceProperty.SaveAndPublish(content);

			}
			catch (System.Exception ex)
			{
				Logger.Error(typeof(HomeResponse), ex);

				return ex.ToString();
			}

			return "OK";
		}

		#endregion
	}

	/// <summary>
	/// Response POCO
	/// </summary>
	public class HomeResponse
	{
		#region Constructors

		/// <summary>
		/// Empty constructor
		/// </summary>
		public HomeResponse() { }

		/// <summary>
		/// Constructor for parsing a response object from the IPublishedContent implementing Home
		/// </summary>
		/// <param name="home">The ModelsBuilder object</param>
		public HomeResponse(Home home)
		{
			this.Name = home.BoCompanyName;
			this.Address = home.BoCompanyAddress;
			this.Phone = home.BoCompanyPhone;
			this.Email = home.BoCompanyEmail;
			this.CVR = home.BoCompanyCvr;
		}

		#endregion

		#region Properties

		public string Name { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		public string CVR { get; set; }

		#endregion
	}
}