﻿using Core.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Core.Controllers
{
    public class FacebookController : SurfaceController
    {
        public ActionResult Callback()
        {
            try
            {
                var code = Request["Code"];
                var facebookAppToken = new FacebookAppToken();

                if (!string.IsNullOrEmpty(code))
                {
                    facebookAppToken = FacebookHelper.GetToken(code);
                }
                // we need to tell backoffice token and are able to display it in controlpanel
                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Hey your token is renewed. New token: " + facebookAppToken.access_token });
                //return null;
            }
            catch (Exception ex)
            {
                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Error in login." });
                //return null;
            }

        }

        public ActionResult FacebookPage()
        {

            var facebookPage = new FacebookPage();
            var setting = InstagramHelper.GetSocialMediaSetting();
            if (setting != null && setting.FacebookUserTokens != null && setting.FacebookUserTokens.Count > 0)
            {
                facebookPage = FacebookHelper.GetFeed(setting.FacebookPageId,setting.FacebookUserTokens.FirstOrDefault().access_token);
            }

            return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { facebookPage = facebookPage });

        }
    }
}
