﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web.Editors;

namespace Core.Controllers
{
    [Umbraco.Web.Mvc.PluginController("My")]
    public class WelcomeApiController : UmbracoAuthorizedJsonController
    {
        public WelcomeApiController()
        {

        }

        public IEnumerable<Person> GetAll()
        {
            var persons = new List<Person>();
            var person = new Person();

            for (int i = 0; i < 6; i++)
            {
                person = new Person();
                person.Id = i + 1;
                person.Name = "Person Name - " + (i + 1);
                person.Town = "Person Town - " + (i + 1);
                person.Country = "Person Country - " + (i + 1);
                persons.Add(person);
            }

            return persons;
        }
    }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Town { get; set; }
    public string Country { get; set; }
}
