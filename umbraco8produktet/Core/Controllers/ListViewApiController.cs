﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.PublishedModels;
using Umbraco.Web.WebApi;

namespace Core.Controllers
{
    public class ListViewApiController : UmbracoApiController
    {
        [HttpGet]
        public List<ListViewApiItem> GetListView(int id)
        {
            var result = new List<ListViewApiItem>();

            //var helper = UmbracoHelper;

            var rootNode = Umbraco.Content(id).Children.FirstOrDefault(x => x is GenericItems);

            foreach (var item in rootNode.Descendants())
            {
                switch (item)
                {
                    case SimpleImageItem imageItem:
                        {
                            var itemTyped = imageItem;
                            var itemToAdd = new ListViewApiItem
                            {
                                heading = itemTyped.Heading,
                                isLink = itemTyped.Link != null,
                                link = itemTyped.Link != null ? itemTyped.Link.Url : string.Empty,
                                type = "image",
                                subheading = itemTyped.SubHeading,
                                text = itemTyped.Text,
                                image = itemTyped.Image.Url,
                            };

                            result.Add(itemToAdd);
                            break;
                        }
                    case SimpleTextItem textItem:
                        {
                            var itemTyped = textItem;
                            var itemToAdd = new ListViewApiItem
                            {
                                heading = itemTyped.Heading,
                                isLink = itemTyped.Link != null,
                                link = itemTyped.Link != null ? itemTyped.Link.Url : string.Empty,
                                type = "image",
                                subheading = itemTyped.SubHeading,
                                text = itemTyped.Text.ToString(),

                            };

                            result.Add(itemToAdd);
                            break;
                        }
                }
            }

            return result;
        }
    }

    public class ListViewApiItem
    {
        public bool isLink { get; set; }
        public string link { get; set; }
        public string type { get; set; }
        public string heading { get; set; }
        public string subheading { get; set; }
        public string text { get; set; }
        public string image { get; set; }
        public string overlaylabel1 { get; set; }
        public string overlaylabel2 { get; set; }
    }

}
