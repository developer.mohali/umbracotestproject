﻿using Core.helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Umbraco.Web.Editors;

namespace Core.Controllers
{
    [Umbraco.Web.Mvc.PluginController("SocialMedia")]
    public class SocialMediaSettingsApiController : UmbracoAuthorizedJsonController
    {
        public SocialMediaSettingsApiController()
        {
            
        }

        [HttpGet]
        public SocialMediaSettingViewModel GetSocialMediaSetting()
        {
            var socialMediaSettingViewModel = new SocialMediaSettingViewModel();
            var socialMediaSetting = InstagramHelper.GetSocialMediaSetting();
            socialMediaSettingViewModel.InstagramAppId = socialMediaSetting.InstagramAppId;
            socialMediaSettingViewModel.InstagramSecretKey = socialMediaSetting.InstagramSecretKey;
            socialMediaSettingViewModel.FacebookAppId = socialMediaSetting.FacebookAppId;
            socialMediaSettingViewModel.FacebookSecretKey = socialMediaSetting.FacebookSecretKey;
            socialMediaSettingViewModel.FacebookPageId = socialMediaSetting.FacebookPageId;
            socialMediaSettingViewModel.IsInstagramValid = false;
            if (socialMediaSetting.InstagramUserTokens != null && socialMediaSetting.InstagramUserTokens.Count > 0)
            {
                var access_token = socialMediaSetting.InstagramUserTokens.FirstOrDefault().access_token;
                if (!string.IsNullOrEmpty(access_token))
                {
                   var userInfo= InstagramHelper.GetUserInform(access_token);
                    if (!string.IsNullOrEmpty(userInfo.id))
                    {
                        socialMediaSettingViewModel.IsInstagramValid = true;
                        socialMediaSettingViewModel.InstagramUserName = userInfo.username;
                    }
                }
            }
            if (socialMediaSetting.FacebookUserTokens != null && socialMediaSetting.FacebookUserTokens.Count > 0)
            {
                var access_token = socialMediaSetting.FacebookUserTokens.FirstOrDefault().access_token;
                if (!string.IsNullOrEmpty(access_token))
                {
                    var userInfo = FacebookHelper.GetUserInform(access_token);
                    if (!string.IsNullOrEmpty(userInfo.id))
                    {
                        socialMediaSettingViewModel.IsFacebookValid = true;
                        socialMediaSettingViewModel.FacebookName = userInfo.name;
                    }
                }
            }

            return socialMediaSettingViewModel;
        }

        [HttpGet]
        public string GetInstagramLoginLink()
        {
            return InstagramHelper.InstagramLoginUrl;
        }

        [HttpGet]
        public string GetFacebookLoginLink()
        {
            return FacebookHelper.FacebookLoginUrl;
        }

    }

    public class SocialMediaSettingViewModel
    {
        public string InstagramAppId { get; set; }
        public string InstagramSecretKey { get; set; }
        public string FacebookAppId { get; set; }
        public string FacebookSecretKey { get; set; }
        public bool IsInstagramValid { get; set; }
        public string InstagramUserName { get; set; }
        public string FacebookName { get; set; }
        public bool IsFacebookValid { get; set; }
        public string FacebookPageId { get; set; }
    }

    public class SocialMediaSetting
    {
        [JsonProperty("instagramAppId")]
        public string InstagramAppId { get; set; }
        [JsonProperty("instagramSecretKey")]
        public string InstagramSecretKey { get; set; }
        [JsonProperty("facebookAppId")]
        public string FacebookAppId { get; set; }
        [JsonProperty("facebookSecretKey")]
        public string FacebookSecretKey { get; set; }
        [JsonProperty("facebookPageId")]
        public string FacebookPageId { get; set; }
        public List<InstagramAppToken> InstagramUserTokens { get; set; }
        public List<FacebookAppToken> FacebookUserTokens { get; set; }

        public DateTime LastSyncDate { get; set; }
    }


}
