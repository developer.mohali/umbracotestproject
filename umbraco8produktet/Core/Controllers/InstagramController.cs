﻿using Core.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;


namespace Core.Controllers
{
    public class InstagramController : SurfaceController
    {
        public ActionResult Callback()
        {
            try
            {
                var code = Request["Code"];
                var instagramtoken = new InstagramAppToken();

                if (!string.IsNullOrEmpty(code))
                {
                    instagramtoken = InstagramHelper.GetToken(code);
                }

                //string DomainName = HttpContext.Current.Request.Url.Host;

                //string test = RazorHelpers.GetAltText()
                EmailSendHelper.SendEmail("formular@dynamikfabrikken.com", "formular@dynamikfabrikken.com", string.Empty, "here Subject", "here body");


                // we need to tell backoffice token and are able to display it in controlpanel
                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Hey your token is renewed. New token: " + instagramtoken.access_token });
                //return null;
            }
            catch (Exception ex)
            {


                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Error in login: " + ex.Message });
            }
        }

        public ActionResult MediaData()
        {

            var instagramMedia = new InstagramMedia();
            var getInstagramSetting = InstagramHelper.GetSocialMediaSetting();
            if (getInstagramSetting != null && getInstagramSetting.InstagramUserTokens != null && getInstagramSetting.InstagramUserTokens.Count > 0)
            {
                instagramMedia = InstagramHelper.GetFeed(getInstagramSetting.InstagramUserTokens.FirstOrDefault().access_token);
            }

            return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { instagramMedia = instagramMedia });

        }
    }

    public class CallbackViewModel
    {
        public string message { get; set; }
        public InstagramMedia instagramMedia { get; set; }

        public FacebookPage facebookPage { get; set; }
    }

}
