﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Umbraco.Web.WebApi;

namespace Core.Controllers
{
    public class SearchApiController : UmbracoApiController
    {
        public object GetSearchResult(string freetext, int size, int page)
        {
            long total = 0;
            var result = Umbraco.ContentQuery.Search(freetext, (page-1) * size, size, out total);
            var resultItems = new List<SearchResultItem>();

            foreach (var item in result)
            {
                var node = Umbraco.Content(item.Content.Id);
                var imageUrl = string.Empty;

                resultItems.Add(new SearchResultItem
                {
                    Body = "",
                    Image = "",
                    Title = item.Content.Name,
                    Url = item.Content.Url
                });
            }

            return resultItems;
        }
    }

    public class SearchResultItem 
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("body")]
        public string Body { get; set; }
    }
}