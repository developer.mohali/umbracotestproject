﻿using ClientDependency.Core;
using Newtonsoft.Json.Linq;
using Core.helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedModels;
using Umbraco.Core.Composing;
using Newtonsoft.Json;

namespace Core.Controllers
{
    public class SocialNewsSaveController : SurfaceController
    {
        public IMedia InsertMediaElement(string fileSource, string socialMediaName)
        {
            try
            {
                string filename = string.Empty;
                Uri uri = new Uri(fileSource);
                filename = Path.GetFileName(uri.LocalPath);
                string type = FileTypeDetectHelper.getFileType(fileSource);
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(fileSource);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;
                WebResponse webResponse = webRequest.GetResponse();
                var stream = webResponse.GetResponseStream();
                IMedia media = Services.MediaService.CreateMediaWithIdentity(socialMediaName, 1104, type);
                media.SetValue(Services.ContentTypeBaseServices, "umbracoFile", filename, stream);
                Services.MediaService.Save(media);
                webResponse.Close();
                return media;
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public JObject buildGridContent(IMedia media, string rteText)
        {

            JObject obj = new JObject();

            var gridContent = new GridContent
            {
                name = "1 column layout",
                sections = new List<Section>()
            };


            List<Object> c = new List<Object>();
            c.Add(new Controls()
            {
                value = "<p>" + rteText + "</p>",
                editor = new GridEditor
                {
                    alias = "rte",
                    name = "Rich text editor",
                    view = "rte",
                    render = null,
                    icon = "icon-article",
                    config = new Config()
                }
            });

            List<Object> ic = new List<Object>();
            ic.Add(new ImageControls()
            {

                value = new ImageValues
                {
                    id = media.Id,
                    uid = media.GetUdi().ToString(),
                    image = media.GetUrl("umbracoFile", Current.Logger),    //"/media/igwjguqx/90402103_648312835993617_8823406426896931429_n.jpg" // media.GetUrl,
                },
                editor = new GridEditor
                {
                    alias = "media",
                    name = "Image",
                    view = "media",
                    render = null,
                    icon = "icon-picture",
                    config = new Config()
                }
            });


            List<Area> a = new List<Area>();
            a.Add(new Area()
            {
                grid = "6",
                styles = null,
                config = null,
                controls = ic

            });

            a.Add(new Area()
            {
                grid = "6",
                styles = null,
                config = null,
                controls = c

            });


            List<Row> Rows = new List<Row>();
            Rows.Add(new Row()
            {
                areas = a,
                name = "Half and half",
                styles = null,
                config = null,
                id = "acf88bb7-abaa-4e70-e7fd-86d89c140852"
            });

            gridContent.sections.Add(
                new Section()
                {
                    grid = "12",
                    rows = Rows
                }
            );

            obj = JObject.FromObject(gridContent);
            return obj;

        }

        //http://produktet.local/umbraco/surface/SocialNewsSave/Sync
        //https://localhost:44328/umbraco/surface/Instagram/MediaData
        //https://localhost:44328/umbraco/surface/Facebook/FacebookPage
        public ActionResult Sync()
        {

            #region "Facebook"
            var facebookPage = new FacebookPage();
            var mediaDataViewModel = new MediaDataViewModel();
            var mediaDataViewModels = new List<MediaDataViewModel>();
            var isValidToken = FacebookHelper.IsValidToken();
            var setting = new SocialMediaSetting();
            if (isValidToken)
            {
                setting = InstagramHelper.GetSocialMediaSetting();
                if (setting != null && setting.FacebookUserTokens != null && setting.FacebookUserTokens.Count > 0)
                {
                    var access_token = setting.FacebookUserTokens.FirstOrDefault().access_token;
                    facebookPage = FacebookHelper.GetFeed(setting.FacebookPageId, access_token);
                    foreach (var item in facebookPage.data)
                    {
                        mediaDataViewModel = new MediaDataViewModel();
                        mediaDataViewModel.PostId = "Facebook_" + item.id;
                        mediaDataViewModel.Source = "Facebook";
                        mediaDataViewModel.Heading = item.message;
                        mediaDataViewModel.Text = item.story;
                        mediaDataViewModel.PostDate = item.created_time;
                        mediaDataViewModel.MediaItems = new List<string>();
                        if (item.attachments != null && item.attachments.data.Count > 0)
                        {
                            foreach (var atta in item.attachments.data)
                            {
                                if (atta.type == "video_inline")
                                {
                                    mediaDataViewModel.MediaItems.Add(atta.media.source);
                                }
                                else if (atta.type != "album")
                                {
                                    mediaDataViewModel.MediaItems.Add(atta.media.image.src);
                                }

                                if (atta.subattachments != null && atta.subattachments.data.Count > 0)
                                {
                                    foreach (var subAtta in atta.subattachments.data)
                                    {
                                        mediaDataViewModel.MediaItems.Add(subAtta.media.image.src);
                                    }
                                }
                            }
                        }
                        mediaDataViewModels.Add(mediaDataViewModel);
                    }
                }
                setting.LastSyncDate = DateTime.Now;
                RazorHelpers.SaveSocialMediaSetting(setting);
            }
            else
            {
                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Update connection to Facebook" });
            }


            #endregion

            #region "Instagram" 
            var instagramMedia = new InstagramMedia();
            InstagramHelper.RefreshLongLiveToken(setting.InstagramUserTokens.FirstOrDefault().access_token);
            isValidToken = InstagramHelper.IsValidToken();
            if (isValidToken)
            {
                if (setting != null && setting.InstagramUserTokens != null && setting.InstagramUserTokens.Count > 0)
                {
                    instagramMedia = InstagramHelper.GetFeed(setting.InstagramUserTokens.FirstOrDefault().access_token);
                }
                foreach (var item in instagramMedia.data)
                {
                    mediaDataViewModel = new MediaDataViewModel();
                    mediaDataViewModel.PostId = "Instagram_" + item.id;
                    mediaDataViewModel.Source = "Instagram";
                    mediaDataViewModel.Heading = item.caption;
                    mediaDataViewModel.Text = item.caption;
                    mediaDataViewModel.PostDate = item.timestamp;
                    mediaDataViewModel.MediaItems = new List<string>();
                    mediaDataViewModel.MediaItems.Add(item.media_url);
                    mediaDataViewModels.Add(mediaDataViewModel);
                }
            }
            else
            {
                return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = "Update connection to Instagram" });
            }

            #endregion
            var message = SyncData(mediaDataViewModels);
            return View("~/Views/Callbackmessages/message.cshtml", new CallbackViewModel { message = message });


        }
        public string SyncData(List<MediaDataViewModel> mediaDataViewModels)
        {
            var jsonGridContent = new JObject();
            int existingItemsCount = 0;
            try
            {
                foreach (var mediaDataView in mediaDataViewModels)
                {
                    string text = mediaDataView.Heading;
                    string heading = string.Empty;
                    int index = 24;
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (text.Length < index)
                        {
                            heading = text;
                        }
                        else
                        {
                            heading = text.Substring(0, index) + "...";
                        }
                    }
                    heading = System.Text.RegularExpressions.Regex.Replace(heading, @"\p{Cs}", "");

                    string postId = mediaDataView.PostId; //will be id on facebook or instagram post we will make it like: "Instagram_ID" and "Facebook_ID"
                    //we need to find parent (ArticleList) by documenttype
                    var contentService = Services.ContentService;
                    var rootNode = contentService.GetRootContent().FirstOrDefault();
                    var newsParent = contentService.GetPagedChildren(rootNode.Id, 0, 100, out long totalRecoreds).FirstOrDefault(x => x.ContentType.Alias.Equals("articleList"));
                    var newselements = Umbraco.Content(newsParent.Id);

                    //foreach social items 

                    existingItemsCount = newselements.Children.Where(x => x.Value("externalId").ToString() == postId).Count();

                    if (existingItemsCount == 0 && !string.IsNullOrEmpty(heading) && (mediaDataView.MediaItems.Count > 0))
                    {

                        //Still a step toDo
                        //make test if news already exist
                        IMedia media = null;
                        foreach (var mediaItem in mediaDataView.MediaItems)
                        {
                            //save the media item
                            media = InsertMediaElement(mediaItem, mediaDataView.Source);
                        }

                        ///make gridContent
                        JObject gridObj = buildGridContent(media, text);

                        //Save the node
                        var newsItem = contentService.Create(heading, newsParent.Id, "article", -1);
                        newsItem.SetValue("articleDate", mediaDataView.PostDate);
                        newsItem.SetValue("externalId", postId);
                        newsItem.SetValue("externalLink", mediaDataView.MediaItems.FirstOrDefault());
                        newsItem.SetValue("isExternal", 1);
                        newsItem.SetValue("boImage", media.GetUdi());
                        newsItem.SetValue("boOverlayHeading", heading);
                        newsItem.SetValue("content", gridObj.ToString());
                        contentService.SaveAndPublish(newsItem);

                    }
                }

                return "Done: " + jsonGridContent + " Exist: " + existingItemsCount;

            }
            catch (Exception ex)
            {
                return "Error on sync: " + ex.Message;

            }
        }
    }

    public class MediaDataViewModel
    {
        public string Text { get; set; }
        public string Heading { get; set; }
        public string PostId { get; set; }
        public List<string> MediaItems { get; set; }
        public string SourceLink { get; set; }
        public string Source { get; set; }
        public DateTime PostDate { get; set; }
    }
    public class GridEditor
    {
        public string alias { get; set; }
        public string name { get; set; }
        public string view { get; set; }
        public string render { get; set; }
        public string icon { get; set; }
        public Config config { get; set; }
    }

    public class Config
    {
    }

    public class ImageValues
    {
        public int id { get; set; }
        public string uid { get; set; }
        public string image { get; set; }
    }

    public class ImageControls
    {
        public ImageValues value { get; set; }

        public GridEditor editor { get; set; }
        public object styles { get; set; }
        public object config { get; set; }
    }

    public class Controls
    {
        public int id { get; set; }
        public string uid { get; set; }
        public string image { get; set; }
        public string value { get; set; }
        public GridEditor editor { get; set; }
        public object styles { get; set; }
        public object config { get; set; }
    }

    public class Area
    {
        public string grid { get; set; }
        public IList<Object> controls { get; set; }
        public object styles { get; set; }
        public object config { get; set; }
    }

    public class Row
    {
        public string name { get; set; }
        public string id { get; set; }
        public List<Area> areas { get; set; }
        public object styles { get; set; }
        public object config { get; set; }
    }

    public class Section
    {
        public string grid { get; set; }
        public IList<Row> rows { get; set; }
    }

    public class GridContent
    {
        public string name { get; set; }
        public IList<Section> sections { get; set; }
    }
}


