﻿using Core.Services;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Core.Composers
{
    public class RegisterSiteServiceComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            // if your service makes use of the current UmbracoContext, eg AssignedContentItem - register the service in Request scope
            // composition.Register<ISiteService, SiteService>(Lifetime.Request);
            // if not then it is better to register in 'Singleton' Scope
            composition.Register<IClientSettingsService, ClientSettingsService>(Lifetime.Request);
        }
    }
}
