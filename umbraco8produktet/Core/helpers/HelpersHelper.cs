﻿using System.Web;

namespace Core.helpers
{
    public class HelpersHelper
    {

        public static string GetCurrentDomainName()
        {
            string BaseUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
            return BaseUrl;
         }

    }
}
