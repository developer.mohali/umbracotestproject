﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Core.helpers
{
    public class EmailSendHelper
    {
        public static string SendEmail(string toEmail, string fromMail, string ccEmail, string emailSubject, string emailBody, string emailSignatur = "", string emailIntroText = "")
        {
            try
            {
                var mail = new MailMessage();
                var client = new SmtpClient("mail.smtp2go.com", 2525)
                {
                    Credentials = new NetworkCredential("formular@dynamikfabrikken.com", "YnhxdXFiaWtvcjAw"),
                    EnableSsl = true
                };

                mail.From = new MailAddress(fromMail);
                if (!string.IsNullOrWhiteSpace(toEmail))
                {
                    foreach (string to in toEmail.Split(';'))
                    {
                        mail.To.Add(new MailAddress(to));
                    }
                }
                if (!string.IsNullOrWhiteSpace(ccEmail))
                {
                    foreach (string cc in ccEmail.Split(';'))
                    {
                        mail.CC.Add(new MailAddress(cc));
                    }
                }

                mail.Subject = emailSubject;


                string FilePath = HttpContext.Current.Server.MapPath("~/Views/Partials/EmailTemplates/EmailTemplate.html");
                StreamReader str = new StreamReader(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();
                emailBody = MailText.Replace("{{dynamictext}}", emailBody).Replace("{{emailsignatur}}", emailSignatur).Replace("{{introtext}}", emailIntroText);


                var plainView = AlternateView.CreateAlternateViewFromString(Regex.Replace(emailBody, @"<[^>]+>|&nbsp;", "").Trim(), null, "text/plain");
                var htmlView = AlternateView.CreateAlternateViewFromString(emailBody, null, "text/html");
                mail.AlternateViews.Add(plainView);
                mail.AlternateViews.Add(htmlView);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return string.Empty;
        }
    }
}
