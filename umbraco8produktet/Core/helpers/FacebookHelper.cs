﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.helpers
{
    public class FacebookHelper
    {
        // set dynamic host //https://produktet.local/umbraco/surface/Facebook/Callback
        public static string FacebookRedirectUri = InstagramHelper.BaseUrl + "umbraco/surface/Facebook/Callback";
        public static string State = "produktet"; // here any string.It is for callback from my request 

        public static string FacebookLoginUrl
        {
            get
            {
                var socialMediaSetting = InstagramHelper.GetSocialMediaSetting();
                var facebookAppId = socialMediaSetting.FacebookAppId;
                return string.Format("https://www.facebook.com/v7.0/dialog/oauth?client_id={0}&redirect_uri={1}&scope=email&state={2}", facebookAppId, FacebookRedirectUri, State);
            }
        }

        public static FacebookAppToken GetToken(string code)
        {

            var request = (HttpWebRequest)WebRequest.Create("https://graph.facebook.com/v7.0/oauth/access_token");
            var socialMediaSetting = InstagramHelper.GetSocialMediaSetting();
            var facebookAppId = socialMediaSetting.FacebookAppId;
            var facebookSecretKey = socialMediaSetting.FacebookSecretKey;
            var postData = "client_id=" + Uri.EscapeDataString(facebookAppId);
            postData += "&client_secret=" + Uri.EscapeDataString(facebookSecretKey);
            postData += "&redirect_uri=" + Uri.EscapeDataString(FacebookRedirectUri);
            postData += "&code=" + Uri.EscapeDataString(code);
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var facebookAppToken = JsonConvert.DeserializeObject<FacebookAppToken>(responseString);
            socialMediaSetting.FacebookUserTokens = new List<FacebookAppToken>();
            socialMediaSetting.FacebookUserTokens.Add(facebookAppToken);
            RazorHelpers.SaveSocialMediaSetting(socialMediaSetting);

            return facebookAppToken;

        }

        public static bool IsValidToken()
        {
            var socialMediaSetting = InstagramHelper.GetSocialMediaSetting();
            if (socialMediaSetting.FacebookUserTokens != null && socialMediaSetting.FacebookUserTokens.Count > 0)
            {
                var access_token = socialMediaSetting.FacebookUserTokens.FirstOrDefault().access_token;
                if (!string.IsNullOrEmpty(access_token))
                {
                    var userInfo = FacebookHelper.GetUserInform(access_token);
                    if (!string.IsNullOrEmpty(userInfo.id))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static FacebookUserInfo GetUserInform(string accessToken)
        {
            var facebookUserInfo = new FacebookUserInfo();
            try
            {
                string getUserData = string.Empty;
                string url = "https://graph.facebook.com/v7.0/me?fields=id,name&access_token=" + accessToken;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    getUserData = reader.ReadToEnd();
                    facebookUserInfo = JsonConvert.DeserializeObject<FacebookUserInfo>(getUserData);
                }
            }
            catch
            {
                facebookUserInfo = new FacebookUserInfo();
            }
            return facebookUserInfo;

        }

        public static FacebookPage GetFeed(string pageId, string accessToken)
        {
            var facebookPage = new FacebookPage();
            string getFacebookData = string.Empty;
            string url = "https://graph.facebook.com/v7.0/" + pageId + "/posts?fields=created_time,message,story,id,picture,full_picture,status_type,attachments&access_token=" + accessToken;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                getFacebookData = reader.ReadToEnd();
                facebookPage = JsonConvert.DeserializeObject<FacebookPage>(getFacebookData);
            }

            return facebookPage;

        }

    }

    public class FacebookAppToken
    {
        [JsonProperty("access_token")]
        public string access_token { get; set; }
        [JsonProperty("token_type")]
        public string token_type { get; set; }
        [JsonProperty("expires_in")]
        public string expires_in { get; set; }
    }

    public class FacebookUserInfo
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
    }




    public class Image
    {
        public int height { get; set; }
        public string src { get; set; }
        public int width { get; set; }

    }

    public class Media
    {
        public Image image { get; set; }
        public string source { get; set; }

    }

    public class Image2
    {
        public int height { get; set; }
        public string src { get; set; }
        public int width { get; set; }

    }

    public class Media2
    {
        public Image2 image { get; set; }

    }

    public class Target
    {
        public string id { get; set; }
        public string url { get; set; }

    }

    public class Datum3
    {
        public Media2 media { get; set; }
        public Target target { get; set; }
        public string type { get; set; }
        public string url { get; set; }

    }

    public class Subattachments
    {
        public List<Datum3> data { get; set; }

    }

    public class Target2
    {
        public string id { get; set; }
        public string url { get; set; }

    }

    public class DescriptionTag
    {
        public string id { get; set; }
        public int length { get; set; }
        public string name { get; set; }
        public int offset { get; set; }
        public string type { get; set; }

    }

    public class Datum2
    {
        public Media media { get; set; }
        public Subattachments subattachments { get; set; }
        public Target2 target { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public List<DescriptionTag> description_tags { get; set; }

    }

    public class Attachments
    {
        public List<Datum2> data { get; set; }

    }

    public class FacebookPageData
    {
        public DateTime created_time { get; set; }
        public string message { get; set; }
        public string id { get; set; }
        public string status_type { get; set; }
        public string story { get; set; }
        public string picture { get; set; }
        public string full_picture { get; set; }
        public Attachments attachments { get; set; }

    }

    

    public class FacebookPage
    {
        public List<FacebookPageData> data { get; set; }
        public Paging paging { get; set; }

    }


}
