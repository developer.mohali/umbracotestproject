﻿using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Snipcart;
using Snipcart.Models;
using System.Net.Http;
using System.Net;
using System.Text;
using System.IO;
using System;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using System.Linq;
using System.Collections.Generic;
using Core.Controllers;
using ImageProcessor.Imaging.Formats;
using System.Drawing;
using ImageProcessor;

namespace Core.helpers
{
    public class RazorHelpers
    {

        public static string GetAltText(string FirstChoiceText, string fallbackText = "")
        {

            string r = string.Empty;

            if (!string.IsNullOrEmpty(FirstChoiceText))
            {
                r = FirstChoiceText;
            }
            else
            {
                r = fallbackText;
            }

            return r;
        }

        public static string CropImageUsingImageProcessor(string file, int width, int height)
        {
            string imgSrc = string.Empty;
            Size size = new Size(width, height);
            file = HttpContext.Current.Server.MapPath(file);
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                {
                    var imgFactory = imageFactory.Load(file)
                                  .Resize(size)
                                  .Save(outStream);
                    var base64 = Convert.ToBase64String(outStream.ToArray());
                    imgSrc = String.Format("data:{0};base64,{1}", imgFactory.CurrentImageFormat.MimeType, base64);
                }
                return imgSrc;
            }
        }

        public static void SaveSocialMediaSetting(SocialMediaSetting socialMediaSetting)
        {
            string socialMediaJson = JsonConvert.SerializeObject(socialMediaSetting);
            File.WriteAllText(HttpContext.Current.Server.MapPath("~/socialmediaData.json"), socialMediaJson);
        }


        public class Imagesizes
        {
            public string BreakPointSettings { get; set; }
            public int imageWidth { get; set; }
            public int imageHeight { get; set; }
        }

        public static string GenerateMediaItemView(IPublishedContent publishedMediaItem,
                                                   IPublishedContent fallbackMediaItem,
                                                   string mediaSizeSettings = @"[{""BreakPointSettings"": ""defaultSettings"", ""imageWidth"": 720, ""imageHeight"": 455},
                                                                                 {""BreakPointSettings"": ""(max-width: 414px)"", ""imageWidth"": 720, ""imageHeight"": 455}, 
                                                                                 {""BreakPointSettings"": ""(max-width: 768px)"", ""imageWidth"": 720, ""imageHeight"": 455}]", bool useWebPImage = false, string fallbackAltTagText = "")
        {



            string strHtml = string.Empty;
            if (publishedMediaItem == null)
            {
                publishedMediaItem = fallbackMediaItem;
            }

            List<Imagesizes> mediaSizes = new List<Imagesizes>();
            mediaSizes = JsonConvert.DeserializeObject<List<Imagesizes>>(mediaSizeSettings);
            var filetype = FileTypeDetectHelper.getFileType(HelpersHelper.GetCurrentDomainName() + publishedMediaItem.Url);

            if (filetype == "Video" || filetype == "File")
            {
                var defaultImageTag = "";
                var imgHtml = "<picture style='opacity: 0;'>";
                foreach (var mediaitem in mediaSizes)
                {
                    if (mediaitem.BreakPointSettings == "defaultSettings")
                    {
                        defaultImageTag = String.Format("<img srcset='{0}' alt=''>", fallbackMediaItem.GetCropUrl(mediaitem.imageWidth, mediaitem.imageHeight));
                    }
                    else
                    {
                        imgHtml += "<source srcset='" + fallbackMediaItem.GetCropUrl(mediaitem.imageWidth, mediaitem.imageHeight) + "' media='" + mediaitem.BreakPointSettings + "'>";
                    }
                }
                imgHtml = imgHtml + defaultImageTag;
                imgHtml = imgHtml + "</picture>";

                strHtml = "<video style='position: absolute; width: 100%; left: 50%; top: 50%; min-width:100 %; min-height: 100%; width: auto; height: auto; transform: translate(-50%, -50%);' playsinline autoplay muted loop>" + String.Format("<source src='{0}' type='video/mp4'>", publishedMediaItem.Url) + "</video>" + imgHtml;

            }
            else
            {
                var defaultImageTag = string.Empty;
                strHtml = "<picture>";
                foreach (var mediaitem in mediaSizes)
                {
                    var imgPath = string.Empty;
                    if (useWebPImage)
                    {
                        imgPath = CropImageUsingImageProcessor(publishedMediaItem.Url, mediaitem.imageWidth, mediaitem.imageHeight);
                    }
                    else
                    {
                        imgPath = publishedMediaItem.GetCropUrl(mediaitem.imageWidth, mediaitem.imageHeight);
                    }
                    if (mediaitem.BreakPointSettings == "defaultSettings")
                    {
                        defaultImageTag = String.Format("<img srcset='{0}' alt='" + GetAltText(publishedMediaItem.Value<string>("altText"), fallbackAltTagText) + "'>", imgPath);
                    }
                    else
                    {
                        strHtml += "<source srcset='" + imgPath + "' media='" + mediaitem.BreakPointSettings + "'>";
                    }
                }
                strHtml = strHtml + defaultImageTag;
                strHtml = strHtml + "</picture>";
            }
            return strHtml;
        }


        //Theme specefic methodes
        //MS
        public static IHtmlString GenerateMsHeading(string heading)
        {
            string r = string.Empty;

            if (!string.IsNullOrEmpty(heading))
            {
                string[] words = heading.Split(' ');
                var count = 0;

                string one = string.Empty;
                string two = string.Empty;
                string three = string.Empty;

                foreach (var word in words)
                {
                    if (count < 3)
                    {
                        one += word + " ";
                    }

                    if (count > 2 && count < 5)
                    {
                        two += word + " ";
                    }

                    if (count > 4)
                    {
                        three += word + " ";
                    }

                    count++;
                }

                r += "<span class='text-indent'>" + one + "</span>";
                r += "<span class='mobile-indent'>" + two + "</span>";
                r += "<span class='mobile-indent mobile-indent--second-level'>" + three + "</span>";

            }

            return new HtmlString(r);
        }

    }
}
