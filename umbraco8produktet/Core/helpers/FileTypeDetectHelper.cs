﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Core.helpers
{
    public class FileTypeDetectHelper
    {
        //https://www.facebook.com/business/help/523719398041952?id=1240182842783684
        private static readonly string[] validImageExtensions = { ".BMP", ".DIB", ".GIF", ".HEIC", ".HEIF", ".IFF", ".JFIF", ".JP2", ".JPE", ".JPG", ".PNG", ".PSD", ".TIF", ".TIFF", ".WBMP", ".WEBP", ".XBM" };
        //https://www.facebook.com/help/218673814818907
        private static readonly string[] validVideoExtensions = { ".3g2", ".3gp", ".3gpp", ".asf", ".avi", ".dat", ".divx", ".dv", ".f4v", ".flv", ".gif", ".m2ts", ".m4v", ".mkv", ".mod", ".mov", ".mp4", "mpe", ".mpeg", ".mpeg4", ".mpg", ".mts", ".nsv", ".ogm", ".ogv", ".qt", ".tod", ".ts", ".vob", ".wmv" };

        public static string getFileType(string fileSource)
        {
            string r = string.Empty;

            Uri uri = new Uri(fileSource);
            string fileext = Path.GetExtension(uri.LocalPath);


            if (validImageExtensions.Contains(fileext.ToUpper()))
            {
                r = "Image";
            }
            if(validVideoExtensions.Contains(fileext.ToLower()))
            {
                r = "Video";
            }

            return r;
        }

    }
}
