﻿using Core.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace Core.helpers
{

    //Long lived token guide:
    // https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens

    // App setup guide:
    // https://developers.facebook.com/docs/instagram-basic-display-api/getting-started/

    public class InstagramHelper
    {
        public static string BaseUrl = HelpersHelper.GetCurrentDomainName();
        public static string InstagramRedirectUri = BaseUrl + "umbraco/surface/Instagram/Callback";
        public static string InstagramLoginUrl
        {
            get
            {
                var socialMediaSetting = GetSocialMediaSetting();
                var instagramAppId = socialMediaSetting.InstagramAppId;
                return string.Format("https://api.instagram.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=user_profile,user_media&response_type=code", instagramAppId, InstagramRedirectUri);
            }
        }
        public static SocialMediaSetting GetSocialMediaSetting()
        {
            var socialMediaSetting = new SocialMediaSetting();
            try
            {
                string socialMediajson = File.ReadAllText(HttpContext.Current.Server.MapPath("~/socialmediaData.json"), Encoding.Default);
                socialMediaSetting = JsonConvert.DeserializeObject<SocialMediaSetting>(socialMediajson);
            }
            catch
            {

            }

            if (socialMediaSetting == null)
            {
                socialMediaSetting = new SocialMediaSetting();
            }
            return socialMediaSetting;
        }
        public static bool IsValidToken()
        {
            var socialMediaSetting = InstagramHelper.GetSocialMediaSetting();
            if (socialMediaSetting.InstagramUserTokens != null && socialMediaSetting.InstagramUserTokens.Count > 0)
            {
                var access_token = socialMediaSetting.InstagramUserTokens.FirstOrDefault().access_token;
                if (!string.IsNullOrEmpty(access_token))
                {
                    var userInfo = InstagramHelper.GetUserInform(access_token);
                    if (!string.IsNullOrEmpty(userInfo.id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static InstagramAppToken GetToken(string code)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://api.instagram.com/oauth/access_token");
            var socialMediaSetting = GetSocialMediaSetting();
            var instagramAppId = socialMediaSetting.InstagramAppId;
            var instagramSecretKey = socialMediaSetting.InstagramSecretKey;
            var postData = "client_id=" + Uri.EscapeDataString(instagramAppId);
            postData += "&client_secret=" + Uri.EscapeDataString(instagramSecretKey);
            postData += "&grant_type=authorization_code";
            postData += "&redirect_uri=" + Uri.EscapeDataString(InstagramRedirectUri);
            postData += "&code=" + Uri.EscapeDataString(code);
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var instagramAppToken = JsonConvert.DeserializeObject<InstagramAppToken>(responseString);
            var token = GetLongLiveToken(instagramSecretKey, instagramAppToken.access_token);
            instagramAppToken.access_token = token;
            socialMediaSetting.InstagramUserTokens = new List<InstagramAppToken>();
            socialMediaSetting.InstagramUserTokens.Add(instagramAppToken);
            RazorHelpers.SaveSocialMediaSetting(socialMediaSetting);
            return instagramAppToken;

        }
        public static InstagramUserInfo GetUserInform(string accessToken)
        {
            var instagramUserInfo = new InstagramUserInfo();
            try
            {
                string getUserData = string.Empty;
                string url = "https://graph.instagram.com/me?fields=id,username&access_token=" + accessToken;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    getUserData = reader.ReadToEnd();
                    instagramUserInfo = JsonConvert.DeserializeObject<InstagramUserInfo>(getUserData);
                }
            }
            catch
            {
                instagramUserInfo = new InstagramUserInfo();
            }
            return instagramUserInfo;

        }
        public static InstagramMedia GetFeed(string accessToken)
        {
            var instagramMedia = new InstagramMedia();
            string getMediaData = string.Empty;
            string url = "https://graph.instagram.com/me/media?fields=id,caption,media_type,media_url,permalink,thumbnail_url,timestamp,username&access_token=" + accessToken;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                getMediaData = reader.ReadToEnd();
                instagramMedia = JsonConvert.DeserializeObject<InstagramMedia>(getMediaData);
            }

            return instagramMedia;

        }
        public static string GetLongLiveToken(string appSecret, string accessToken)
        {
            var instagramAppToken = new InstagramAppToken();
            if (!string.IsNullOrEmpty(accessToken) && !string.IsNullOrEmpty(appSecret))
            {
                try
                {
                    string getUserData = string.Empty;
                    string url = string.Format("https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret={0}&access_token={1}", appSecret, accessToken);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.AutomaticDecompression = DecompressionMethods.GZip;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        getUserData = reader.ReadToEnd();
                        instagramAppToken = JsonConvert.DeserializeObject<InstagramAppToken>(getUserData);
                    }
                }
                catch
                {
                    instagramAppToken = new InstagramAppToken();
                }
            }
            return instagramAppToken.access_token;
        }

        public static string RefreshLongLiveToken(string accessToken)
        {
            var instagramAppToken = new InstagramAppToken();
            if (!string.IsNullOrEmpty(accessToken))
            {
                try
                {
                    string getUserData = string.Empty;
                    string url = string.Format("https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token={0}", accessToken);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.AutomaticDecompression = DecompressionMethods.GZip;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        getUserData = reader.ReadToEnd();
                        instagramAppToken = JsonConvert.DeserializeObject<InstagramAppToken>(getUserData);
                        var socialMediaSetting = GetSocialMediaSetting();
                        socialMediaSetting.InstagramUserTokens = new List<InstagramAppToken>();
                        instagramAppToken.access_token = instagramAppToken.access_token;
                        socialMediaSetting.InstagramUserTokens.Add(instagramAppToken);
                        RazorHelpers.SaveSocialMediaSetting(socialMediaSetting);
                       
                    }
                }
                catch (Exception ex)
                {
                    instagramAppToken = new InstagramAppToken();
                }
            }
            return instagramAppToken.access_token;
        }
    }

    public class InstagramAppToken
    {
        [JsonProperty("access_token")]
        public string access_token { get; set; }
        [JsonProperty("user_id")]
        public string user_id { get; set; }
    }

    public class InstagramUserInfo
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("username")]
        public string username { get; set; }
    }

    public class InstagramMediaData
    {
        public string id { get; set; }
        public string caption { get; set; }
        public string media_type { get; set; }
        public string media_url { get; set; }
        public string permalink { get; set; }
        public DateTime timestamp { get; set; }
        public string username { get; set; }

        public string thumbnail_url { get; set; }
    }

    public class Cursors
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Paging
    {
        public Cursors cursors { get; set; }
        public string next { get; set; }
    }

    public class InstagramMedia
    {
        public IList<InstagramMediaData> data { get; set; }
        public Paging paging { get; set; }
    }


}
