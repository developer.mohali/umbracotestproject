﻿angular.module("umbraco").controller("SocialMediaController", function ($scope, $window, socialMediaResource) {
    var vm = this;
    vm.instagramForm = {
        appid: '',
        secretKey: '',
        isValid: false,
        userName: ''
    };
    vm.facebookForm = {
        appid: '',
        secretKey: '',
        isValid: false,
        userName: '',
        pageId:''
    };
    vm.isAppIdAndkey = false;
    vm.isFacebookAppIdAndkey = false;
    getInstagramDetail();

    function getInstagramDetail() {
        socialMediaResource.getSocialMediaSetting().then(function (response) {
            if (response !== null) {
                vm.instagramForm.appid = response.InstagramAppId;
                vm.instagramForm.secretKey = response.InstagramSecretKey;
                vm.facebookForm.appid = response.FacebookAppId;
                vm.facebookForm.secretKey = response.FacebookSecretKey;
                vm.instagramForm.isValid = response.IsInstagramValid;
                vm.instagramForm.userName = response.InstagramUserName;
                vm.facebookForm.isValid = response.IsFacebookValid;
                vm.facebookForm.userName = response.FacebookName;
                vm.facebookForm.pageId = response.FacebookPageId;
            }

            if (vm.instagramForm.appid !== null && vm.instagramForm.secretKey !== null && vm.instagramForm.appid.length > 0 && vm.instagramForm.secretKey.length > 0) {
                vm.isAppIdAndkey = true;
            }
            if (vm.facebookForm.appid !== null && vm.facebookForm.secretKey !== null && vm.facebookForm.appid.length > 0 && vm.facebookForm.secretKey.length > 0) {
                vm.isFacebookAppIdAndkey = true;
            }
        });
    }

   

   


});


