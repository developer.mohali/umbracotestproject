﻿function serialize(form) {
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i, j, q = [];
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === "") {
            continue;
        }
        switch (form.elements[i].nodeName) {
            case 'INPUT':
                switch (form.elements[i].type) {
                    case 'text':
                    case 'tel':
                    case 'email':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                    case 'checkbox':
                    case 'radio':
                        if (form.elements[i].checked) {
                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        }
                        break;
                }
                break;
            case 'file':
                break;
            case 'TEXTAREA':
                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                break;
            case 'SELECT':
                switch (form.elements[i].type) {
                    case 'select-one':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                    case 'select-multiple':
                        for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                            if (form.elements[i].options[j].selected) {
                                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
                            }
                        }
                        break;
                }
                break;
            case 'BUTTON':
                switch (form.elements[i].type) {
                    case 'reset':
                    case 'submit':
                    case 'button':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                }
                break;
        }
    }
    return q.join("&");
}
function ValidationContactForm() {
    var error = true;
    $('#contact-form input[type=text]').each(function (index) {
        if (index == 0) {
            if ($(this).val() == null || $(this).val() == "") {
                $("#contact-form").find("input:eq(" + index + ")").addClass("required-error");
                error = false;
            } else {
                $("#contact-form").find("input:eq(" + index + ")").removeClass("required-error");
            }
        } else if (index == 1) {
            if (!(/(.+)@(.+){2,}\.(.+){2,}/.test($(this).val()))) {
                $("#contact-form").find("input:eq(" + index + ")").addClass("required-error");
                error = false;
            } else {
                $("#contact-form").find("input:eq(" + index + ")").removeClass("required-error");
            }
        }

    });
    return error;
}
function submitForm(formId) {
    var error = ValidationContactForm();
    if (error) {

        var form = document.getElementById(formId);
        var successMessage = form.getAttribute("data-success");
        var failMessage = form.getAttribute("data-fail");
        var waitMessage = form.getAttribute("data-wait");
        var data = serialize(form) + "&emailSubject=" + form.getAttribute("data-email-subject") + "&emailto=" + form.getAttribute("data-email-to") + "&emailfrom=" + form.getAttribute("data-email-from") + "&emailtemplate=" + form.getAttribute("data-template") + "&emailsignatur=" + form.getAttribute("data-signatur-markup") + "&emailintro=" + form.getAttribute("data-intro-text-markup") + "&lang=" + form.getAttribute("data-language");

        //Add overlay
        var overlay = document.createElement('div');
        overlay.className = "form-overlay";
        overlay.innerHTML = waitMessage;
        form.appendChild(overlay);


        //Now send to Umbraco....    
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', '/umbraco/api/Formular/sumbitFormular', true);
        xmlhttp.setRequestHeader('Content-Type', 'application/json');

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    overlay.innerHTML = successMessage;
                    //console.log(xmlhttp);
                    console.log(xmlhttp.responseText);
                }
                else {
                    overlay.innerHTML = failMessage;
                }
            }
        };
        xmlhttp.send(JSON.stringify(data));
    }
}


