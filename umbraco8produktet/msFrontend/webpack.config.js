const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    main: path.resolve(__dirname, 'src/ts/main.ts'),
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'build/ts'),
    filename: 'tsCompiled.js'
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        loaders: ['babel-loader', 'awesome-typescript-loader'],
        exclude: /node_modules/
      }
    ]
  },
  plugins: []
};