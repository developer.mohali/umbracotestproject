$(document).ready(function () {
  initSections()

  function initSections() {
    $('.api-cards.images-section__grid-container').each(function (index, element) {

      const imagesSection = {
        searchApiUrl: "./scripts/fake_images_response.json",
        pageSize: 12,
        nextPage: 1,
        contentContainer: $(element),
        isLoading: false,


        enableLazyScroll: function () {
          const wrap = this.contentContainer.closest('section')[0];
          $(window).scroll(function () {
            if (imagesSection.isLoading) return;

            if (wrap.offsetTop + wrap.clientHeight <= window.pageYOffset + document.documentElement.clientHeight) {
              imagesSection.loadNextPage()
            }
          })
        },


        loadNextPage: function () {
           imagesSection.isLoading = true;



          if (window.location.hostname === "localhost" && window.location.port === '5050') {
              if (this.nextPage > 3) return;
          }
          else {
              this.searchApiUrl = $(this.contentContainer.closest('section').find(".images-section__grid-container")[0]).data("api-url");

              console.log("I am in");
              console.log($(this.contentContainer.closest('section').find(".images-section__grid-container")[0]).data("api-url"));

              if (this.searchApiUrl.indexOf('?') !== -1) {
                  this.searchApiUrl += "&size=" + this.pageSize;
              }
              else {
                  this.searchApiUrl += "?size=" + this.pageSize;
              }
              this.searchApiUrl += "&page=" + this.nextPage;
          }

            console.log("orginal searchapi: " + this.searchApiUrl);
          this.nextPage++;

          $.getJSON(this.searchApiUrl, function (data) {
            imagesSection.addImages(data)
            const currentData = []
            const storedData = JSON.parse(localStorage.getItem('imagesSectionData'))
            $.each(storedData, function (index, el) {
              currentData.push(el)
            })
            const newData = $.merge(data, currentData)
            localStorage.setItem('imagesSectionData', JSON.stringify(newData))
          })
        },


        addImages: function (data) {
          const lastIndex = document.querySelectorAll('.images-section-item').length

          $.each(data, function (index, item) {
            index += lastIndex;

            const wrap = (item.type === 'image') ? imagesSection.createImageItem(item, index) : imagesSection.createTextItem(item, index);
            imagesSection.contentContainer.append(wrap);

            $(window).on('scroll.' + index, function () {
              imagesSection.animateImageAppear(wrap, index)
            })
          })

          imagesSection.isLoading = false;
        },


        animateImageAppear: function (image, index) {
          const top_of_element = $(image).offset().top;
          const bottom_of_element = $(image).offset().top + $(image).outerHeight();
          const bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
          const top_of_screen = $(window).scrollTop();

          if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
            setTimeout(function() {
              $(image).removeClass('images-section-item_hidden')
            }, Math.random() / 2 * 1000)
            $(window).off('scroll.' + index)
          }
        },


        createImageItem: function (item, index) {
          const wrapEl = item.isLink ? 'a' : 'div'
          const wrap = document.createElement(wrapEl);
          wrap.className = 'images-section-item images-section-item_hidden images-section-image-item';
          wrap.href = item.link;
          wrap.onclick = function () {
            if (!item.isLink) return;
            window.shouldDeleteImagesSectionData = false
          }

          this.setupGridPosition(wrap, index)
          this.setupIEGrid(wrap, index)

          const imgWrp = document.createElement('div');
          imgWrp.className = 'images-section-image-item__image'
          const img = document.createElement('img')
          img.src = item.image;
          $(imgWrp).append(img);

          const imgOverlay = document.createElement('div');
          imgOverlay.className = 'images-section-image-item__overlay';
          $(imgWrp).append(imgOverlay);

          $(wrap).append(imgWrp);


          const label1 = document.createElement('span');
          label1.className = 'images-section-image-item__label1'
          label1.textContent = item.overlaylabel1;

          const label2 = document.createElement('span');
          label2.className = 'images-section-image-item__label2'
          label2.textContent = item.overlaylabel2;
          $(wrap).append(label2);

          const infoSection = document.createElement('div');
          infoSection.className = 'images-section-image-item__info-section';

          $(infoSection).append(label1);

          const title = document.createElement('h3');
          title.className = 'images-section-image-item__title';
          title.textContent = item.heading;
          $(infoSection).append(title);

          const subtitle = document.createElement('h4');
          subtitle.className = 'images-section-image-item__subtitle';
          subtitle.textContent = item.subheading;
          $(infoSection).append(subtitle);

          const description = document.createElement('p');
          description.className = 'images-section-image-item__description';
          description.textContent = item.text;
          $(infoSection).append(description);

          const infoSectionOverlay = document.createElement('div');
          infoSectionOverlay.className = 'images-section-image-item__info-section__overlay';
          $(infoSection).append(infoSectionOverlay);


          $(wrap).append(infoSection);
          

          $(img).on("load", function () {
            imagesSection.setupCardHover(wrap, img, infoSection, description)
          })

          return wrap;
        },


        createTextItem: function (item, index) {
          const wrapEl = item.isLink ? 'a' : 'div'
          const wrap = document.createElement(wrapEl);
          wrap.className = 'images-section-item images-section-item_hidden images-section-text-item';
          randomNumber = Math.floor(1 + Math.random() * 3)
          wrap.classList.add('images-section-text-item_color_' + randomNumber)
          wrap.href = item.link;
          wrap.onclick = function () {
            window.shouldDeleteImagesSectionData = false
          }

          this.setupGridPosition(wrap, index)
          this.setupIEGrid(wrap, index)

          const contentContainer = document.createElement('div');
          contentContainer.className = 'images-section-text-item__container'

          const title = document.createElement('h3');
          title.className = 'images-section-text-item__title';
          title.textContent = item.heading;
          $(contentContainer).append(title);

          const subtitle = document.createElement('h4');
          subtitle.className = 'images-section-text-item__subtitle';
          subtitle.textContent = item.subheading;
          $(contentContainer).append(subtitle);

          const description = document.createElement('p');
          description.className = 'images-section-text-item__description';
          description.textContent = item.text;
          $(contentContainer).append(description);

          const button = document.createElement('a');
          button.className = "images-section-text-item__button"
          button.href = item.buttonLinkUrl;
          button.target = item.buttonLinkTarget;
          button.textContent = item.buttonLinkText;
          $(contentContainer).append(button);

          $(wrap).append(contentContainer);
          return wrap;
        },


        setupIEGrid: function (wrap, index) {
          if (!this.contentContainer.closest('.images-section__grid-container').css('msGridColumns')) return;
          $(window).resize(function () {
            imagesSection.setupIEGrid(wrap, index)
          })

          const IEColumns = this.contentContainer.closest('.images-section__grid-container').css('msGridColumns')
          if (IEColumns) {
            const IEColumnsCount = IEColumns.split(' ').length
            const rowNumber = Math.ceil((index + 1) / IEColumnsCount)
            wrap.style.msGridRow = rowNumber

            if (this.contentContainer.closest('.images-section-wrap_type_simple').length) {
              wrap.style.marginLeft = '10%'
              wrap.style.marginRight = '10%'
              wrap.style.marginBottom = '70px'
            }
            if (this.contentContainer.closest('.images-section-wrap_type_card').length) {
              wrap.style.marginLeft = '10%'
              wrap.style.marginRight = '10%'
              wrap.style.marginBottom = '70px'
            }
            if (this.contentContainer.closest('.images-section-wrap_type_grid').length) {
              wrap.style.marginLeft = '5px'
              wrap.style.marginRight = '5px'
              wrap.style.marginBottom = '10px'
            }

            return
          }
        },


        setupGridPosition: function (wrap, index) {
          if (!this.contentContainer.closest('.images-section-wrap_type_grid').length) return;
          if (this.contentContainer.closest('.images-section__grid-container').css('msGridColumns')) return;

          const gridPosition = (index + 1) % 8
          if (gridPosition === 3) {
            $(wrap).addClass('images-section-item_grid_big')
          }

          if ($(wrap).hasClass('images-section-text-item')) {
            $(wrap).addClass('images-section-item_grid_tall')
            return;
          }

          if (gridPosition === 6) {
            $(wrap).addClass('images-section-item_grid_tall')
          }
        },


        setupCardHover: function (wrap, img, infoSection, description) {
          if (!$(wrap).closest('.images-section-wrap_type_card').length) return;

          let descriptionHeight = $(description).outerHeight();
          let cardHeight = $(img).outerHeight() + $(infoSection).outerHeight()
          $(description).css('height', '0')
          $(wrap).css('height', cardHeight - descriptionHeight + 'px')

          $(wrap).hover(function () {
            $(this).find('.images-section-image-item__description').css({
              height: descriptionHeight + 'px',
              opacity: '1'
            })
            $(this).find('.images-section-image-item__info-section').css({ "transform": "translateY(-" + descriptionHeight + "px)" })
          }, function () {
            $(this).find('.images-section-image-item__description').css({
              height: '0',
              opacity: '0'
            })
            $(this).find('.images-section-image-item__info-section').css({ "transform": "translateY(0)" })
          });


          let shouldResize = false;

          setInterval(function () {
            if (shouldResize) {
              shouldResize = false
              $(description).css('height', '')
              $(wrap).css('height', '')
              descriptionHeight = $(description).outerHeight();
              cardHeight = $(img).outerHeight() + $(infoSection).outerHeight()
              $(description).css('height', '0')
              $(wrap).css('height', cardHeight - descriptionHeight + 'px')
            }
          }, 500)

          $(window).resize(function () {
            shouldResize = true
          })
        },


        setupImagesRestoring: function () {
          window.shouldDeleteImagesSectionData = true
          $(window).on('unload', function () {
            if (window.shouldDeleteImagesSectionData) {
              localStorage.removeItem('imagesSectionData')
            }
          })

          const imagesSectionData = JSON.parse(localStorage.getItem('imagesSectionData'));

          if (imagesSectionData) {
            this.isLoading = true;
            let elementsCount = 0;
            $.each(imagesSectionData, function () {
              elementsCount++;
            })
            this.nextPage = (elementsCount / imagesSection.pageSize + 1);
            this.addImages(imagesSectionData);
          }
        },


        init: function () {
          this.setupImagesRestoring()
          this.enableLazyScroll()
        }
      }

      imagesSection.init()
    })
  }
})
