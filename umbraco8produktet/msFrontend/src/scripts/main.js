$(document).ready(function() {
    var body = $('body');
    var width = $(window).width();
    var headerLineAnimation = {
        headerLine: $('#scroll-line'),
        headerLineText: $('#scroll-line-text'),
        marker: true,
        controlAnimation: true,
        lineShow: function(element) {
            if (headerLineAnimation.controlAnimation) {
                headerLineAnimation.controlAnimation = false;
                $.Velocity.animate(element, {
                    width: "100%",
                }, {
                    duration: 700,
                    delay: 200,
                    easing: [0.05, 0.78, 1, 1],
                }).then(function() {
                    headerLineAnimation.headerLineText.css('display', 'inline-block');
                    headerLineAnimation.textShow();
                });
            }
        },
        textShow: function() {
            $.Velocity.animate(this.headerLineText, "transition.fadeIn", {
                duration: 300
            }).then(function() {
                headerLineAnimation.controlAnimation = true;
            })
        },
        textHidden: function(delay) {
            if (headerLineAnimation.controlAnimation) {
                headerLineAnimation.controlAnimation = false;
                $.Velocity.animate(this.headerLineText, { opacity: 0 }, {
                    duration: 300,
                }).then(function() {
                    headerLineAnimation.headerLineText.css('display', 'none');
                    headerLineAnimation.lineHidden(headerLineAnimation.headerLine, delay);
                })
            }
        },
        lineHidden: function(element, delay) {
            var delayTime = delay;

            if (!delayTime) {
                delayTime = 700
            }
            $.Velocity.animate(element, {
                width: "0%",
            }, {
                duration: delayTime,
                easing: [0.05, 0.78, 1, 1]
            }).then(function() {
                headerLineAnimation.marker = true;
                headerLineAnimation.controlAnimation = true;
            })
        },
        scroll: function(element) {
            $(window).on('scroll', function() {
                if ($(window).scrollTop() > 0 && headerLineAnimation.marker) {
                    headerLineAnimation.lineShow(element);
                    headerLineAnimation.marker = false;
                } else if ($(window).scrollTop() === 0 && !headerLineAnimation.marker) {
                    headerLineAnimation.textHidden();

                }
            })
        }
    };
    var headerBtn = {
        menuBtn: $('.menu-btn-wrap'),
        menui: $('.icon-menu'),
        mapi: $('.icon-map-icon'),
        mapBtn: $('.search-btn-wrp'),
        showIcon: function() {
            $(this.menui).velocity({ opacity: 1 }, { easing: 'easeIn', delay: 0 });
            $(this.mapi).velocity({ opacity: 1 }, { easing: 'easeIn', delay: 0 });
        },
        scaleLoad: function() {
            $(this.menuBtn).velocity({ scale: 1 }, { easing: 'easeIn', delay: 2000 });
            $.Velocity.animate($(this.mapBtn), { scale: 1 }, { easing: 'easeIn', delay: 2000 }).then(function() {
                headerLineAnimation.scroll(headerLineAnimation.headerLine);
                headerBtn.showIcon();
            })
        }
    };
    var heroSection = {
        logo: $('.logo'),
        banner: $('.js-animation-banner'),
        downArrow: $('.btn-scroll-down'),
        body: $('body'),
        bannerShow: function() {
            var self = this;
            $.Velocity.animate(this.banner, {
                opacity: 1
            }, {
                easing: 'linear',
                delay: 600,
                duration: 800,
                progress: function(start) {
                    if (start) {
                        self.body.addClass('blocked');
                    }
                }
            }).then(function(elements) {
                if (elements) {
                    heroSection.showLogo();
                    heroSection.showArrow();
                }
            });
        },
        showLogo: function() {
            var self = this;
            $.Velocity.animate(this.logo, {
                opacity: 1,
            }, {
                easing: 'easeIn',
                delay: 500
            }).then(function() {
                self.body.removeClass('blocked')
            })
        },
        showArrow: function() {
            var self = this;
            this.downArrow.velocity({
                opacity: 1,
            }, {
                easing: 'easeIn',
                delay: 500
            })
        },
        minScale: function() {

            $.Velocity.animate($('#hero-img-bord-left'), {
                scale: 7,
            }, {
                easing: 'linear',
                duration: 300,
            });

            $.Velocity.animate($('#hero-img-bord-right'), {
                scale: 7,
            }, {
                easing: 'linear',
                duration: 300,
            });

            $.Velocity.animate($('#hero-img-bord-top'), {
                scale: 7,
            }, {
                easing: 'linear',
                duration: 300,
            });

            $.Velocity.animate($('#hero-img-bord-bot'), {
                scale: 7,
            }, {
                easing: 'linear',
                duration: 300,
            });
        },
        defaultScale: function() {

            $.Velocity.animate($('#hero-img-bord-left'), {
                scale: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 500
            });

            $.Velocity.animate($('#hero-img-bord-right'), {
                scale: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 500
            });

            $.Velocity.animate($('#hero-img-bord-bot'), {
                scale: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 500
            });

            $.Velocity.animate($('#hero-img-bord-top'), {
                scale: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 500
            });
        },
    };
    var menu = {
        menuBtn: $('.menu-btn-wrap'),
        menuWrapper: $('.menu-wrp'),
        leftPart: $('.left-part'),
        rightPart: $('#right-content'),
        menuLogo: $('.menu-logo'),
        menu: $('.menu'),
        openIconClass: 'icon-close',
        signUpBtn: $('#sign-up'),
        signUpForm: $('.sign-up-form'),
        hiddenText: $('#text-hidden'),

        signUpFormShow: function() {
            if ($(this).hasClass('show')) {
                $(this).removeClass('show');
                menu.signUpForm.slideUp();
            } else {
                $(this).addClass('show');
                menu.signUpForm.slideDown();
            }
        },

        slideMenu: function() {
            var self = this;
            $.Velocity.animate(self.menuWrapper, {
                translateX: "0%"
            }, {
                easing: 'linear',
                delay: 300,
            }).then(function() {
                menu.slideLeftPart();
                setTimeout(function() {
                    $("footer, section, #scroll-line").hide();
                }, 300);
            })
        },

        slideLeftPart: function() {
            var self = this;
            $.Velocity.animate(self.leftPart, {
                translateX: '0%',
            }, {
                easing: 'linear',
                delay: 300,
                progress: function(elements, complite, start) {
                    if (start) {
                        menu.menuBtn.addClass('trans-bg');

                        if (width < 1024) {
                            $('.search-btn-wrp').addClass('trans-bg');
                        }
                    }
                }
            }).then(function() {
                menu.showMenuLogo();
                menu.showLeftContent();
                menu.showRightContent();
            })
        },

        showMenuLogo: function() {
            $.Velocity.animate(this.menuLogo, {
                opacity: 1,
            }, {
                easing: 'linear',
                delay: 200,
                duration: 400,
            });
        },

        showLeftContent: function() {
            $.Velocity.animate(this.menu, {
                opacity: 1,
            }, {
                easing: 'linear',
                duration: 450,
                delay: 250
            })
        },

        showRightContent: function() {
            $.Velocity.animate(this.rightPart, {
                opacity: 1,
            }, {
                easing: 'linear',
                duration: 500,
                delay: 250
            }).then(function() {
                menu.menuBtn.prop('disabled', false);
            })
        },

        hiddenMenuLogo: function() {
            this.menuBtn.prop('disabled', true);
            $.Velocity.animate(this.menuLogo, {
                opacity: 0,
            }, {
                easing: 'linear',
                delay: 150,
                duration: 150,
            }).then(function() {
                menu.hiddenLeftContent();
                menu.hiddenRightContent();
            });
        },

        hiddenLeftContent: function() {
            $.Velocity.animate(this.menu, {
                opacity: 0,
            }, {
                easing: 'linear',
                duration: 250,
                delay: 200
            })
        },

        hiddenRightContent: function() {
            $.Velocity.animate(this.rightPart, {
                opacity: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 250
            }).then(function() {
                menu.hiddenLeftPart();
            })
        },

        hiddenLeftPart: function() {
            $.Velocity.animate(this.leftPart, {
                translateX: '-300%',
            }, {
                easing: 'linear',
                duration: 150,
            }).then(function() {
                menu.menuBtn.removeClass('trans-bg');
                if (width < 1024) {
                    $('.search-btn-wrp').removeClass('trans-bg');
                }
                menu.hiddenOveraly();
            })
        },

        hiddenOveraly: function() {
            var self = this;
            if ($(window).scrollTop() !== 0) {
                headerLineAnimation.lineShow(headerLineAnimation.headerLine);
            }
            $.Velocity.animate(self.menuWrapper, {
                translateX: "100%"
            }, {
                easing: 'linear',
                delay: 200,
            }).then(function() {
                heroSection.defaultScale();
                menu.menuBtn.prop('disabled', false);
                $("footer, section, #scroll-line").show();
            })
        },

        hiddenMenu: function() {
            this.hiddenMenuLogo();
        },

        clickBtn: function() {
            if ($(this).hasClass('icon-closes')) {
                $(this).removeClass('icon-closes');
                menu.hiddenMenu();
                body.removeClass('blocked');
                menu.signUpBtn.removeClass('show');
                menu.hiddenText.slideDown();
                menu.signUpForm.slideUp();
            } else {
                $(this).addClass('icon-closes').prop('disabled', true);
                body.addClass('blocked');
                heroSection.minScale();
                headerLineAnimation.textHidden(100);
                menu.slideMenu();
            }
        },

        init: function() {
            this.menuBtn.on('click', this.clickBtn);
            this.signUpBtn.on('click', this.signUpFormShow)
        }
    };



    var search = {
        searchBtn: $('#searchBtn'),
        closeBtn: $('#searchBtnClose'),
        menuBtn: $('.menu-btn-wrap'),
        searchWrapper: $('.search-wrp'),
        leftPart: $('#search__left-content'),
        rightPart: $('.search-wrp__right-part'),
        openIconClass: 'icon-close',
        hiddenText: $('#text-hidden'),
        searchForm: $('.search-form'),
        searchApiUrl: "./scripts/fake_search_response.json",
        pageSize: 4,
        nextPage: 1,
        searchInput: $('#searchInput'),
        contentContainer: $('#search__right-content'),
        loader: $('.search__loading'),
        submitTimeoutId: null,


        slidesearch: function() {
            var self = this;
            $.Velocity.animate(self.searchWrapper, {
                translateX: "0%"
            }, {
                easing: 'linear',
                delay: 300,
            }).then(function() {
                search.slideRightPart();
                setTimeout(function() {
                    $("footer, section, #scroll-line").hide();
                }, 300);
            })
        },

        slideRightPart: function() {
            var self = this;
            $.Velocity.animate(self.rightPart, {
                translateX: '0%',
            }, {
                easing: 'linear',
                delay: 300,
                progress: function(elements, complite, start) {
                    if (start) {
                        search.searchBtn.addClass('trans-bg');

                        if (width < 1024) {
                            $('.search-btn-wrp').addClass('trans-bg');
                        }
                    }
                }
            }).then(function() {
                search.showLeftContent();
                search.showRightContent();
            })
        },

        showRightContent: function() {
            $.Velocity.animate(this.search, {
                opacity: 1,
            }, {
                easing: 'linear',
                duration: 450,
                delay: 250
            })
        },

        showLeftContent: function() {
            $.Velocity.animate(this.leftPart, {
                opacity: 1,
            }, {
                easing: 'linear',
                duration: 500,
                delay: 250
            }).then(function() {
                search.searchBtn.prop('disabled', false);
                search.searchInput.focus()
            })
        },


        hiddenRightContent: function() {
            $.Velocity.animate(this.search, {
                opacity: 0,
            }, {
                easing: 'linear',
                duration: 250,
                delay: 200
            })
        },

        hiddenLeftContent: function() {
            $.Velocity.animate(this.leftPart, {
                opacity: 0,
            }, {
                easing: 'linear',
                duration: 300,
                delay: 250
            }).then(function() {
                search.hiddenRightPart();
            })
        },

        hiddenRightPart: function() {
            $.Velocity.animate(this.rightPart, {
                translateX: '-300%',
            }, {
                easing: 'linear',
                duration: 150,
            }).then(function() {
                search.searchBtn.removeClass('trans-bg');
                if (width < 1024) {
                    $('.search-btn-wrp').removeClass('trans-bg');
                }
                search.hiddenOveraly();
            })
        },

        hiddenOveraly: function() {
            var self = this;
            if ($(window).scrollTop() !== 0) {
                headerLineAnimation.lineShow(headerLineAnimation.headerLine);
            }
            $.Velocity.animate(self.searchWrapper, {
                translateX: "100%"
            }, {
                easing: 'linear',
                delay: 200,
            }).then(function() {
                heroSection.defaultScale();
                search.searchBtn.prop('disabled', false);
                $("footer, section, #scroll-line").show();
                self.menuBtn.show();
                self.searchBtn.show();
            })
        },


        clickBtn: function() {
            if (search.searchBtn.hasClass('icon-closes')) {
                search.searchBtn.removeClass('icon-closes');
                body.removeClass('blocked');
                search.hiddenText.slideDown();
                search.hiddenLeftContent()
                search.hiddenRightContent()
                body.removeClass('blocked');
                menu.signUpBtn.removeClass('show');
                menu.hiddenText.slideDown();
                menu.signUpForm.slideUp();
                search.closeBtn.hide();

            } else {
                search.searchBtn.addClass('icon-closes').prop('disabled', true);
                body.addClass('blocked');
                heroSection.minScale();
                headerLineAnimation.textHidden(100);
                search.slidesearch();
                search.menuBtn.hide();
                search.searchBtn.hide();
                search.closeBtn.show();
            }
        },

        startSearch: function(event) {
            event.preventDefault();
            var searchText = search.searchInput.val();

            if (!searchText) return;
            if (search.submitTimeoutId) {
                clearTimeout(search.submitTimeoutId)
            }

            search.contentContainer.removeClass('content-wrp__active')
            search.contentContainer.empty()
            localStorage.removeItem('searchData')
            search.nextPage = 1;
            search.loader.show()
            search.loadNextPage()
            search.setupLazyScroll()
        },

        setupLazyScroll: function() {
            var contentWrap = $('#search__right-content').get()[0]
            var searchWrap = search.searchWrapper.get()[0]
            contentWrap.onscroll = function() {
                if (contentWrap.scrollTop + contentWrap.clientHeight >= contentWrap.scrollHeight) {
                    search.loadNextPage()
                }
            }
            searchWrap.onscroll = function() {
                if (searchWrap.scrollTop + searchWrap.clientHeight >= searchWrap.scrollHeight) {
                    search.loadNextPage()
                }
            }
        },

        loadNextPage: function() {
            if (window.location.hostname !== "localhost" && window.location.port !== '5050') {
                search.searchApiUrl = $("#searchInput").data("api-url");
                search.searchApiUrl += "?size=" + search.pageSize;
                search.searchApiUrl += "&page=" + search.nextPage;
                search.searchApiUrl += "&freetext=" + search.searchInput.val();
            }
            search.nextPage++;

            $.getJSON(search.searchApiUrl, function(data) {
                search.loader.hide()
                search.showSearchResults(data)
                var currentData = []
                var storedData = JSON.parse(localStorage.getItem('searchData'))
                $.each(storedData, function(index, el) {
                    currentData.push(el)
                })
                var newData = $.merge(data, currentData)
                localStorage.setItem('searchData', JSON.stringify(newData))
            })
        },

        showSearchResults: function(results) {
            search.contentContainer.addClass('content-wrp__active')

            $.each(results, function(index, item) {
                var div = document.createElement('div');
                div.className = 'search-block-info';

                var img = document.createElement('img');
                img.className = 'search-block-info__image'
                img.src = item.image;
                $(div).append(img);

                var infoContainer = document.createElement('div');
                infoContainer.className = 'search-block-info__info-container'

                var title = document.createElement('h2');
                title.className = 'search-block-info__title'
                title.textContent = item.title;
                $(infoContainer).append(title)

                var text = document.createElement('p')
                text.className = 'search-block-info__text'
                text.textContent = item.body;
                $(infoContainer).append(text)

                $(div).append(infoContainer)

                var link = document.createElement('a');
                link.href = item.url;
                link.onclick = function(event) {
                    window.shouldDeleteSearchData = false
                }
                $(link).append(div)

                $(search.contentContainer).append(link)
            })
        },

        autoSubmit: function() {
            if (search.submitTimeoutId) {
                clearTimeout(search.submitTimeoutId)
            }
            if (document.querySelector('.search-block-info')) {
                search.contentContainer.empty()
            }
            search.loader.show()

            search.submitTimeoutId = setTimeout(function() {
                search.searchForm.submit()
            }, 2000)
        },

        setupDataRestoring: function() {
            window.shouldDeleteSearchData = true
            $(window).on('unload', function() {
                if (window.shouldDeleteSearchData) {
                    localStorage.removeItem('searchData')
                }
            })
            var searchData = JSON.parse(localStorage.getItem('searchData'));
            if (searchData) {
                var elementsCount = 0;
                $.each(searchData, function() {
                    elementsCount++;
                })
                search.nextPage = (elementsCount / search.pageSize + 1)
                this.showSearchResults(searchData)

                this.searchWrapper.css('transition', 'none')
                this.searchWrapper.css('transform', 'translateX(0%)')
                setTimeout(function() { search.searchWrapper.css('transition', '') })
                $("footer, section, #scroll-line").hide();
                this.rightPart.css('transform', 'translateX(0%)')
                search.searchBtn.addClass('trans-bg');
                if (width < 1024) {
                    $('.search-btn-wrp').addClass('trans-bg');
                }
                this.leftPart.css('opacity', '1')
                search.searchBtn.prop('disabled', false);
                setTimeout(function() { $('#btn-up').removeAttr('style') }) +
                    search.searchBtn.addClass('icon-closes').prop('disabled', true);
                body.addClass('blocked');
                heroSection.minScale();
                headerLineAnimation.textHidden(100);
                search.menuBtn.hide();
                search.searchBtn.hide();
                search.closeBtn.show();
                search.searchInput.focus()

                search.setupLazyScroll()
            }
        },

        init: function() {
            this.searchBtn.on('click', this.clickBtn);
            this.closeBtn.on('click', this.clickBtn);
            this.searchForm.submit(this.startSearch);
            this.loader.hide();
            this.closeBtn.hide();
            this.searchInput.on('input', this.autoSubmit)
            this.setupDataRestoring()
        }
    };

    //bg image loaded
    function getBgUrl(el) {
        var bg = "";
        if (el.currentStyle) { // IE
            bg = el.currentStyle.backgroundImage;
        } else if (document.defaultView && document.defaultView.getComputedStyle) { // Firefox
            bg = document.defaultView.getComputedStyle(el, "").backgroundImage;
        } else { // try and get inline style
            bg = el.style.backgroundImage;
        }
        return bg.replace(/url\(['"]?(.*?)['"]?\)/i, "$1");
    }

    //align my backgroundheight...
    function fitsectionsheights() {
        $.each($('.fit-overlay-content'), function() {
            $(this).parent().find(".fit-overlay-overlay").css("height", $(this).outerHeight());
        })
    }

    //banner height
    function fitbannerheight() {
        $.each($('.intro .overlay-intro-text, .intro .tool-overlay,.intro .godt-overlay'), function() {
            var bannerelm = $(this).closest(".intro").find(".intro__text");
            $(this).css("height", bannerelm.outerHeight());
            $(this).closest(".intro").height(bannerelm.offset().top + bannerelm.outerHeight());

            if (bannerelm.outerHeight() > 0) {
                var image = document.createElement('img');
                image.src = getBgUrl($('.intro__bg')[0]);
                image.onload = function() {
                    $("#tadaoverlay").hide();
                    $("#tada").velocity({ opacity: 0 }, {
                        easing: 'linear',
                        duration: 1000,
                        delay: 0
                    });
                }
            };
        });
    }

    if ($('.intro').length) {
        fitbannerheight();
        fitsectionsheights();
    }
    //window resize
    if (window.attachEvent) {
        window.attachEvent('onresize', function() {
            fitbannerheight();
            fitsectionsheights();
        });
    } else if (window.addEventListener) {
        window.addEventListener('resize', function() {
            fitbannerheight();
            fitsectionsheights();
        }, true);
    }


    heroSection.downArrow.click(function() {
        $('html, body').animate({
            scrollTop: ($(".content_wrapper_sections").children("section").eq(1).offset().top - 280)
        }, 2000);
    });

    /*Scroll */
    var $window = $(window);
    var scrollTime = 1;
    var scrollDistance = 800;

    if ($("body").attr('data-scrolltime')) {
        scrollTime = $("body").data("scrolltime");
    }
    if ($("body").attr('data-scrollistance')) {
        scrollDistance = $("body").data("scrolldistance");
    }


    function bodyScroll(event) {
        if (!menu.menuBtn.hasClass('icon-closes') && !search.searchBtn.hasClass('icon-closes')) {

            event.preventDefault();

            var delta = event.wheelDelta / 120 || -event.detail / 3;
            var scrollTop = $window.scrollTop();
            var finalScroll = scrollTop - parseInt(delta * scrollDistance);

            TweenMax.to($window, scrollTime, {
                scrollTo: { y: finalScroll, autoKill: true },
                ease: Power1.easeOut,
                overwrite: 5
            });

        }
    }

    document.body.addEventListener('mousewheel', bodyScroll, { passive: false });
    document.body.addEventListener('DOMMouseScroll', bodyScroll, { passive: false });

    /* /Scroll*/

    /*Animation Home page*/
    /*Animation*/
    var controller = new ScrollMagic.Controller();

    var InfoImages = $('.text-left-image-video-right').map(function() {
        var img = $(this).find('.text-left-image-video-right__img')
        var lBoard = $(this).find('.section-img-l-board')
        var tBoard = $(this).find('.section-img-t-board')
        return TweenMax.to([img, lBoard, tBoard], 2, {
            scale: 1,
            y: 0
        })
    })
    var InfoTexts = $('.text-left-image-video-right__text').map(function(index, text) {
        return TweenMax.to(text, 3, {
            y: 0,
        });
    })

    var timelineImgTexts = $('.image-overlay-left-text-bgimage-right').map(function() {
        var lBoard = $(this).find('.section-img-l-board')
        var rBoard = $(this).find('.section-img-r-board')
        var tBoard = $(this).find('.section-img-t-board')
        var imgDesc = $(this).find('.img-desc')
        var timelineImgText = new TimelineLite();
        return timelineImgText.to([lBoard, rBoard], 2, {
                scaleX: 0,
            })
            .to(tBoard, 2, {
                scaleY: 0,
            }, 0)
            .to(imgDesc, 4, {
                y: 0,
            }, 0.5);
    })

    var timeLineSliderOverlays = $('.slider-section').map(function() {
        var lBoard = $(this).find('.section-img-l-board')
        var rBoard = $(this).find('.section-img-r-board')
        var tBoard = $(this).find('.section-img-t-board')
        var timeLineSliderOverlay = new TimelineLite();
        return timeLineSliderOverlay.to([lBoard, rBoard], 2, {
                scaleX: 0,
            })
            .to(tBoard, 2, {
                scaleY: 0,
            }, 0);
    })

    var ekologyTimeLines = $('.image-text-overlay').map(function() {
        var lBoard = $(this).find('.section-img-l-board')
        var rBoard = $(this).find('.section-img-r-board')
        var tBoard = $(this).find('.section-img-t-board')
        var text = $(this).find('.ecology-text')
        var ekologyTimeLine = new TimelineLite();
        return ekologyTimeLine.to([lBoard, rBoard], 2, { scaleX: 0 })
            .to(tBoard, 2, { scaleY: 0 }, 0)
            .to(text, 8, { y: -60 });
    })

    var mapTexts = $('.image-overlay-left-text-bgimage-right').map(function() {
        var text = $(this).find('.image-overlay-left-text-bgimage-right__text')
        return TweenMax.to(text, 1, {
            opacity: 1,
        });
    })


    var boys = $('.slider-section').map(function() {
        var boy = $(this).find('.boy-img-wrp')
        return TweenMax.to(boy, 5, {
            y: -550
        });
    })

    var sliderTextsTranslate = $('.slider-section').map(function() {
        var sliderText = $(this).find('.slider-section__text')
        return TweenMax.to(sliderText, 5, {
            y: -100
        });
    })

    var sliderTextsOpacity = $('.slider-section').map(function() {
        var sliderText = $(this).find('.slider-section__text')
        return TweenMax.to(sliderText, 5, {
            opacity: 1
        });
    })

    var marketImages = $('.market-section').map(function() {
        var marketImg = $(this).find('.market-section__img')
        return TweenMax.to(marketImg, 2, { y: 0 });
    })

    var contactImgOwerlays = $('.contact-section').map(function() {
        var lBoard = $(this).find('.section-img-l-board')
        var rBoard = $(this).find('.section-img-r-board')
        var tBoard = $(this).find('.section-img-t-board')
        var contactImgOwerlay = new TimelineLite();
        return contactImgOwerlay.to([lBoard, rBoard], 4, { scaleX: 0 })
            .to(tBoard, 4, { scaleY: 0 }, 0);
    })

    var marketTexts = $('.market-section').map(function() {
        var marketText = $(this).find('.market-text')
        return TweenMax.to(marketText, 2, { y: -100 });
    })

    var contactTexts = $('.contact-section').map(function() {
        var contactText = $(this).find('.contact-section__text')
        return TweenMax.to(contactText, 4, { opacity: 1 });
    })

    var contactImgTexts = $('.contact-section').map(function() {
        var contactImgText = $(this).find('.contact-img-text')
        return TweenMax.to(contactImgText, 5, { y: 0 });
    })

    var sliderControls = $('.slider-section').map(function() {
        var controlSlider = $(this).find('.slider-control')
        return TweenMax.to(controlSlider, 1, { scaleX: 1 });
    })

    var durationInfoImg;
    var s3offset = -15;
    var s0offset = -350;
    var s5offset = -200;

    if (width < 1024) {
        s3offset = -340;
        s0offset = -150;
        s5offset = -400;
    }

    if (width > 1200) {
        durationInfoImg = 900;
    } else {
        durationInfoImg = 600;
    }


    $('.animation-anchor.s0').each(function(index) {
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: durationInfoImg,
                offset: s0offset
            }).setTween(InfoImages[index])
            /* .addIndicators({name: 'Video'})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1250,
                offset: -350
            }).setTween(InfoTexts[index])
            /*.addIndicators({name: 'Video text'})*/
            .addTo(controller);
    })


    $('.animation-anchor.s1').each(function(index) {
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1000,
                offset: s3offset
            }).setTween(timelineImgTexts[index])
            /*.addIndicators({name: 'Find img'})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 400,
                offset: 300
            }).setTween(mapTexts[index])
            /*.addIndicators({name: "Map Text"})*/
            .addTo(controller);
    })


    $('.animation-anchor.s3').each(function(index) {
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 490,
                offset: s5offset
            }).setTween(timeLineSliderOverlays[index])
            /*.addIndicators({name: "Slider"})*/
            .addTo(controller);

        var offsetControlSlider;
        if (width > 1440) {
            offsetControlSlider = 300;
        } else {
            offsetControlSlider = 300;
        }

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 80,
                offset: offsetControlSlider
            }).setTween(sliderControls[index])
            /*.addIndicators({name: "Slider Control"})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1800,
                offset: 290
            }).setTween(boys[index])
            /* .addIndicators({name: "boy"})*/
            .addTo(controller);


        var offsetTextSlider;
        if (width > 1200) {
            offsetTextSlider = 800;
        } else {
            offsetTextSlider = 500;
        }

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 500,
                offset: offsetTextSlider
            }).setTween(sliderTextsTranslate[index])
            /*.addIndicators({name: "Text"})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 700,
                offset: 500
            }).setTween(sliderTextsOpacity[index])
            /*.addIndicators({name: "Text"})*/
            .addTo(controller);
    })


    $('.animation-anchor.s4').each(function(index) {
        var s8 = new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1200,
                offset: -250
            }).setTween(ekologyTimeLines[index])
            /*.addIndicators({name: "Ecology"})*/
            .addTo(controller);
    })


    $('.animation-anchor.s5').each(function(index) {
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1000,
                offset: -270
            }).setTween(marketImages[index])
            /*.addIndicators({name: "marketOverlay"})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 1000,
                offset: 0
            }).setTween(marketTexts[index])
            /*.addIndicators({name: "marketText"})*/
            .addTo(controller);
    })

    $('.animation-anchor.s6').each(function(index) {
        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 500,
                offset: 0
            }).setTween(contactImgOwerlays[index])
            /*.addIndicators({name: "contactO"})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 500,
                offset: 50
            }).setTween(contactTexts[index])
            /*.addIndicators({name: "contactT"})*/
            .addTo(controller);

        new ScrollMagic.Scene({
                triggerElement: this,
                duration: 700,
                offset: 250
            }).setTween(contactImgTexts[index])
            /*.addIndicators({name: "contactIT"})*/
            .addTo(controller);
    })

    /* /Animation Home page*/

    /*Slider Home page*/
    if ($("#slider").length != 0) {
        /*Slider*/
        $('.slider').each(function() {
            var slick = $(this).slick({
                appendArrows: $(this).closest('.slider-section').find('.slider-control'),
                prevArrow: $(this).closest('.slider-section').find('.slick-prev'),
                nextArrow: $(this).closest('.slider-section').find('.slick-next'),
            });

            var cSlide = $(this).closest('.slider-section').find('.c-slide');
            var aSlides = $(this).closest('.slider-section').find('.a-slides');
            var cS = slick.slick('slickCurrentSlide');
            var gSlick = slick.slick('getSlick');

            aSlides.text(gSlick.$slides.length);
            cSlide.text(cS + 1);

            slick.on('afterChange', function(event, slick, currentSlide) {
                cSlide.text(currentSlide + 1);
            });
        })
    }
    /*Slider Home page*/

    var markerTime = true;

    $("#bgvid").on("timeupdate", function() {
        var timeVideo = Math.round(this.duration * 1000);
        var cTime = Math.floor(this.currentTime);
        if (cTime === 0 && markerTime) {
            markerTime = false;
            bar(timeVideo);
        }

    });

    function bar(duration) {
        $('.time-line').each(function() {
            var bar = new ProgressBar.Circle(this, {
                strokeWidth: 50,
                easing: 'linear',
                duration: duration,
                color: '#FFF',
                svgStyle: null
            });

            bar.animate(1, function() {
                bar.destroy();
                markerTime = true;
            });
        });
    }

    /*Main Init*/
    heroSection.bannerShow();
    headerBtn.scaleLoad();
    menu.init();
    search.init();

    //Validation email
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('#submit-btn').on('click', function(event) {
        var email = $('#email');
        var emailVal = email.val();
        if (!validateEmail(emailVal)) {
            event.preventDefault();
            email.addClass('no-valid');
        }
    });

    $('#email').on('keydown', function() {
        if ($(this).hasClass('no-valid')) {
            $(this).removeClass('no-valid');
        }
    });
    // /Validation email


    setTimeout(function() {
        $('.popup').css('display', 'flex');
    }, 1000);

    $('#close-popup').on('click', function(event) {
        event.preventDefault();
        $('.popup').removeAttr('style');
    });

    $('.popuo-overlay').on('click', function() {
        $('.popup').removeAttr('style');
    });

    $('#next').on('click', function() {
        $('.second-step').removeClass('hidden-input');
        $('.first-step').addClass('hidden-input')
    });

    var playYoutubeButton = $('.js-play-y-video'),
        currentPlayer,
        currentPlayerIndex,
        players = [],
        aspectRatio = 0.5625;
    // aspectRatio = 1.78;

    var YTPlayer = {

        init: function() {
            this.addListener();
        },

        addListener: function(event) {
            this._listeners.initPlayer(event);
            this._listeners.setIframeContainerHeight();
            playYoutubeButton.on('click', event, this._listeners.playVideo);
            $window.on('resize', event, this._listeners.setIframeSize);
            $window.on('resize', event, this._listeners.setIframeContainerHeight);
        },

        _listeners: {

            initPlayer: function(event) {
                // 2. This code loads the IFrame Player API code asynchronously.
                var tag = document.createElement('script');

                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            },

            playVideo: function(event) {

                var areaForInsertingVideo = $(this).next(),
                    videoId = $(this).attr('data-videoid');

                currentPlayer = $(this);
                currentPlayerIndex = playYoutubeButton.index(currentPlayer);

                if (currentPlayer.hasClass('played')) {
                    players[currentPlayerIndex].pauseVideo();
                    currentPlayer.removeClass('played');
                    currentPlayer.addClass('paused');
                } else if (currentPlayer.hasClass('paused')) {
                    playYoutubeButton.each(function(index) {
                        if ($(this).hasClass('played')) {
                            $(this).removeClass('played').addClass('paused');
                            players[index].pauseVideo();
                        }
                    });
                    players[currentPlayerIndex].playVideo();
                    currentPlayer.removeClass('paused');
                    currentPlayer.addClass('played');
                } else {

                    var player = new YT.Player(areaForInsertingVideo[0], {
                        width: '320',
                        height: '180',
                        videoId: videoId,
                        playerVars: { 'showinfo': 0, 'controls': 0 },
                        events: {
                            'onReady': YTPlayer._listeners.onPlayerReady,
                            'onStateChange': YTPlayer._listeners.onPlayerStateChange
                        }
                    });
                    players[currentPlayerIndex] = player;
                    playYoutubeButton.each(function(index) {
                        if ($(this).hasClass('played')) {
                            $(this).removeClass('played').addClass('paused');
                            players[index].pauseVideo();
                        }
                    });
                    currentPlayer.addClass('played');
                }

            },

            setIframeContainerHeight: function() {
                playYoutubeButton.each(function() {

                    var iframeWidth = $(this).outerWidth(),
                        newParentHeight = iframeWidth * aspectRatio;

                    $(this).parent().css({ "min-height": newParentHeight + "px", "max-height": newParentHeight + "px", "height": newParentHeight + "px" });
                });
            },

            setIframeSize: function(event) {

                if (currentPlayer) {
                    var youtubeFrames = $("iframe[src^='https://www.youtube.com']");

                    youtubeFrames.each(function() {

                        var iframeWidth = $(this).outerWidth(),
                            newParentHeight = iframeWidth * aspectRatio;

                        $(this).parent().css({ "min-height": newParentHeight + "px", "max-height": newParentHeight + "px", "height": newParentHeight + "px" });
                    });
                }
            },

            onPlayerReady: function(event) {

                players[currentPlayerIndex].playVideo();

                var iframe = $(event.target.a),
                    iframeWidth = iframe.outerWidth(),
                    newParentHeight = iframeWidth * aspectRatio;

                iframe.parent().css({ "min-height": newParentHeight + "px", "max-height": newParentHeight + "px", "height": newParentHeight + "px" });
            },

            onPlayerStateChange: function(event) {
                if (event.data === YT.PlayerState.ENDED) {
                    players[currentPlayerIndex].destroy();
                    currentPlayer.removeClass('played');
                }
            }
        }
    };

    if (playYoutubeButton) YTPlayer.init();

    /*Scroll top btn*/
    var triggerSection = $('section:last').offset().top - 1000;
    $(window).on('scroll load', function() {

        if ($(window).scrollTop() > triggerSection && !$('#searchBtn').hasClass('icon-closes')) {
            $('#btn-up').css({ opacity: 1, visibility: 'visible' });
        } else {
            $('#btn-up').removeAttr('style');
        }

    });

    $('#btn-up').on('click', function() {
        $('html, body').animate({ scrollTop: 0 }, 1000);
        return false;
    });

});